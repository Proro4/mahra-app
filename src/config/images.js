export const imagesTheme = {
  light: {
    logo: require('src/assets/images/Mahra-logo.png'),
    // coupon: require('src/assets/images/coupon.png'),
    wave: require('src/assets/images/wave.png'),
  },
  dark: {
    logo: require('src/assets/images/logo-dark.png'),
    // coupon: require('src/assets/images/coupon-dark.png'),
    wave: require('src/assets/images/wave-dark.png'),
  },
};
