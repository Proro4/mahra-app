import React from 'react';

import {languageSelector} from 'src/modules/common/selectors';
import {compose} from 'recompose';
import {connect} from 'react-redux';
import {withNavigation} from 'react-navigation';

import {StyleSheet, View} from 'react-native';
import {withTheme, Image, Text} from 'src/components';
import {Row, Col} from 'src/containers/Gird';
import Quantity from 'src/containers/Quantity';

import {grey4} from 'src/components/config/colors';
import {padding, margin} from 'src/components/config/spacing';
import {sizes, lineHeights} from 'src/components/config/fonts';

import currencyFormatter from 'src/utils/currency-formatter';
import logger from '../../../utils/logger';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {wrapMapToPropsFunc} from "react-redux/lib/connect/wrapMapToProps";

class CartItem extends React.Component {
  onChange = quantity => {
    const {changeQuantity, item, size} = this.props;
    changeQuantity(item, quantity);
  };

  renderVariation = (item, index) => {
    return (
      <Text key={index} style={styles.textAttribute}>
        {item.name} : {item.option}
      </Text>
    );
  };

  renderImage = product => {
    let source =
      product.thumbnails && product.thumbnails.length
        ? {uri: product.thumbnails[0].url}
        : require('src/assets/images/pDefault.png');

    // if (
    //   product.type === 'variable' &&
    //   variation.image &&
    //   variation.image.woocommerce_single
    // ) {
    //   source = {uri: variation.image.woocommerce_single};
    // }

    return <Image source={source} style={styles.image} />;
  };

  getPrice = () => {
    return currencyFormatter(this.props.item.price, 'UAH');
  };

  render() {
    const {theme, item, goToProduct, style, t, lang} = this.props;
    const {quantity, variation, buy_step, buy_min} = item;
    logger.log('buy_min', buy_min)
    return (
      <Row
        style={[
          styles.container,
          {
            backgroundColor: theme.colors.bgColor,
            borderColor: theme.colors.border,
          },
          style && style,
        ]}>
        <TouchableWithoutFeedback onPress={() => goToProduct(item)}>
          {this.renderImage(item, variation)}
        </TouchableWithoutFeedback>
        <Col style={styles.content}>
          <View>
            <Text medium onPress={() => goToProduct(item)} style={styles.title}>
              {item
                ? `${
                    lang !== 'ua'
                      ? item.i18ns[0]?.name || item.name
                      : item.i18ns[1]?.name || item.name
                  } ${item.size && t('empty:text_size') + ':'} ${item.size}`
                : ''}
            </Text>
            {/* <Row style={styles.viewAttribute}>
              {meta_data.map((item, index) =>
                this.renderVariation(item, index),
              )}
            </Row> */}
          </View>
          {!item.sold_individually && (
              <Quantity
                min_quantity={parseInt(buy_step || '1', 10)}
                value={quantity}
                buy_min={buy_min}
                onChange={this.onChange}
              />
          )}
        </Col>

        <Text medium onPress={() => goToProduct(item)}>
          {this.getPrice()}
        </Text>
      </Row>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginLeft: 0,
    marginRight: 0,
    padding: padding.large,
    borderBottomWidth: 1,
  },
  image: {
    width: 107,
    height: 107,
  },
  content: {
    paddingLeft: padding.big,
    paddingRight: padding.big,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  title: {
    marginBottom: margin.small - 1,
  },
  viewAttribute: {
    marginBottom: margin.small,
    marginLeft: 0,
    marginRight: 0,
    flexWrap: 'wrap',
  },
  textAttribute: {
    fontSize: sizes.h6 - 2,
    lineHeight: lineHeights.h6 - 2,
    color: grey4,
    marginRight: margin.small,
  },

});

const mapStateToProps = state => {
  return {
    lang: languageSelector(state),
  };
};

// export default withTheme(CartItem);
export default compose(
  withTheme,
  withNavigation,
  connect(mapStateToProps),
)(CartItem);
