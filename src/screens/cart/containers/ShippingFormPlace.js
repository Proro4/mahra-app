import React from 'react';
import {withTranslation} from 'react-i18next';
import {Map} from 'immutable';

import {View, StyleSheet} from 'react-native';

import {Row, Col} from 'src/containers/Gird';
import Input from 'src/containers/input/Input';

import {margin} from 'src/components/config/spacing';
import AsyncStorage from '@react-native-community/async-storage';

class ShippingFormPlace extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      address: '',
    };
  }

  async getAddress() {
    const res = await AsyncStorage.getItem('@address');
    this.setState({address: res});
  }

  componentDidMount() {
    this.getAddress();
  }

  render() {
    const {onChange, data, errors, t} = this.props;
    if (!data || !data.size) {
      return null;
    }

    return (
      <View>
        <Row style={styles.row}>
          <Col>
            <Input
              error={errors && errors.get('address_1')}
              label={t('inputs:text_address')}
              value={data.get('address_1')}
              onChangeText={value => onChange('address_1', value)}
            />
            <Input
              error={errors && errors.get('address_1')}
              label={t('inputs:comment')}
              value={data.get('note')}
              onChangeText={value => onChange('note', value)}
            />
          </Col>
        </Row>
      </View>
    );
  }
}

ShippingFormPlace.defaultProps = {
  errors: Map(),
};

const styles = StyleSheet.create({
  row: {
    marginBottom: margin.base,
  },
});

export default withTranslation()(ShippingFormPlace);
