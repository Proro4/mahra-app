import React from 'react';
import {compose} from 'redux';
import {withNavigation} from 'react-navigation';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';

import {StyleSheet, View} from 'react-native';
import {withTheme, Text, Avatar} from 'src/components';

import Container from 'src/containers/Container';
import Button from 'src/containers/Button';

import {homeTabs} from 'src/config/navigator';

import {white} from 'src/components/config/colors';
import {margin} from 'src/components/config/spacing';
import {lineHeights} from 'src/components/config/fonts';
import {clearCart} from 'src/modules/cart/actions';
import {languageSelector} from '../../../modules/common/selectors';

class Done extends React.Component {
  handleContinue = () => {
    const {navigation} = this.props;
    this.props.dispatch(clearCart());
    navigation.pop();
    navigation.navigate(homeTabs.shop);
  };

  render() {
    const {theme, t, language} = this.props;
    console.log('this.props', language);
    return (
      <Container style={styles.container}>
        <View style={styles.content}>
          <Avatar
            rounded
            icon={{
              name: 'check',
              size: 47,
              color: white,
            }}
            size={95}
            overlayContainerStyle={{
              backgroundColor: theme.colors && theme.colors.success,
            }}
            containerStyle={styles.icon}
          />
          <Text h2 medium style={styles.textTitle}>
            {t('cart:text_congrats')}
          </Text>
          {this.props.params.response && language !== 'ua' && (
            <Text colorSecondary style={styles.textDescription}>
              Спасибо за ваш заказ! Спасибо, {this.props.params.response.fio}!
              Ваша заявка принята. Ваш номер № {this.props.params.response.id}{' '}
              Мы свяжемся с вами в ближайшее время для подтверждения вашего
              заказа. Ожидайте звонка! Все заказы, подтвержденные и оплаченные
              до 20:00, будут отправлены только на следующий день и в такие дни
              как: понедельник, вторник, среда, четверг, пятница, суббота.
            </Text>
          )}
          {this.props.params.response && language === 'ua' && (
            <Text colorSecondary style={styles.textDescription}>
              Дякую за ваше замовлення! Спасибі,{' '}
              {this.props.params.response.fio}! Ваша заявка прийнята. Ваш номер
              № {this.props.params.response.id} Ми зв'яжемося з вами найближчим
              часом для підтвердження вашого замовлення. Чекайте дзвінка! Всі
              замовлення, підтверджені і оплачені до 20:00, будуть відправлені
              тільки на наступний день і в такі дні як: понеділок, вівторок,
              середа, четвер, п'ятниця, субота.
            </Text>
          )}
        </View>
        <Button
          title={t('cart:text_shopping')}
          onPress={this.handleContinue}
          containerStyle={styles.button}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    marginBottom: margin.big + 4,
  },
  textTitle: {
    marginBottom: margin.base,
  },
  textDescription: {
    textAlign: 'center',
    lineHeight: lineHeights.h4,
  },
  button: {
    marginVertical: margin.big,
  },
});

const mapStateToProps = state => {
  return {
    language: languageSelector(state),
  };
};

export default compose(
  withTranslation(),
  withNavigation,
  withTheme,
  connect(mapStateToProps),
)(Done);
