import React from 'react';
import {View, ActivityIndicator, ScrollView, StyleSheet} from 'react-native';

import forEach from 'lodash/forEach';
import {connect} from 'react-redux';

import {Text} from 'src/components';
import ChooseItem from 'src/containers/ChooseItem';
import {fetchGetShippingMethod} from 'src/modules/cart/service';

import {getContinentCode, getZones} from 'src/modules/cart/service';
import {
  shippingMethodNotCoveredByZoneSelector,
  currencySelector,
  defaultCurrencySelector,
  listCurrencySelector,
  languageSelector,
} from 'src/modules/common/selectors';

import currencyFormatter from 'src/utils/currency-formatter';

import {margin, padding} from 'src/components/config/spacing';
import {
  red,
  blue,
  teal,
  yellow,
  pink,
  olive,
  orange,
  violet,
  purple,
  brown,
} from 'src/components/config/colors';
import {calcCost} from 'src/utils/product';
import logger from '../../../utils/logger';

const color = [
  red,
  blue,
  teal,
  yellow,
  pink,
  olive,
  orange,
  violet,
  purple,
  brown,
];

class ShippingMethod extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: false,
      data: [],
      shippingData: [],
    };
  }

  componentDidMount() {
    // const {cc} = this.props;
    // this.getShipping(cc);
    this.getData();
  }

  componentDidUpdate(prevProps) {
    // const {cc} = this.props;
    // if (cc !== prevProps.cc) {
    //   this.getShipping(cc);
    // }
  }
  getData = async () => {
    const {language} = this.props;
    logger.log('language', language);
    this.setState({
      loading: true,
    });
    try {
      const res = await fetchGetShippingMethod();
      const res_data =
        res.items &&
        res.items.map(value => {
          if (language === 'ua') {
            const country_data = value.i18ns[1];
            return {
              ...value,
              ...country_data,
            };
          } else {
            const country_data = value.i18ns[0];
            return {
              ...value,
              ...country_data,
            };
          }
        });
      this.setState({shippingData: res_data.reverse()});
    } catch (e) {
      logger.log('err', e);
    } finally {
      this.setState({
        loading: false,
      });
    }
  };

  /**
   * Get shipping methods
   * @param cc: country code
   * @returns {Promise<void>}
   */
  getShipping = async cc => {
    try {
      if (cc) {
        this.setState({
          loading: true,
        });

        const continent = await getContinentCode(cc);
        const zones = await getZones();
        let check = [];

        forEach(zones, ({zone_locations, shipping_methods}) => {
          // Check Zone regions
          check = zone_locations.filter(({code, type}) => {
            if (
              (type === 'continent' && code === continent) ||
              (type === 'country' && code === cc)
            ) {
              return true;
            }
            return false;
          });

          if (check.length) {
            this.setState({
              loading: false,
              data: shipping_methods,
            });
            // exist forEach
            return false;
          }
        });

        if (!check.length) {
          const {methodsNotCoveredByZone} = this.props;
          const {loading, data} = methodsNotCoveredByZone.toJS();
          this.setState({
            loading,
            data,
          });
        }
      }
    } catch (err) {
      console.log(err);
      this.setState({
        loading: false,
      });
    }
  };

  // Get currency by rate
  getCurrencyByRate = cost => {
    const {currencies, defaultCurrency, currency} = this.props;
    // const currencyData = currencies.get(currency);

    // calc rate when user change currency
    // if (currency !== defaultCurrency) {
    //   return currencyData.get('rate') * cost;
    // }
    return cost;
  };

  // render shipping method
  renderItem = ({item, index}) => {
    const {onChangeShippingMethod, selected, currency} = this.props;
    const cost = calcCost(item);
    // const rate = this.getCurrencyByRate(cost);
    // const topElement = (
    //   <Text h3 medium>
    //     {currencyFormatter(rate, currency)}
    //   </Text>
    // );
    const bottomElement = (
      <View style={styles.footerItem}>
        <Text h3 medium style={{color: color[index], marginRight: margin.base}}>
          {item.title}
        </Text>
        {/*<Text h6 colorThird>*/}
        {/*  10 days*/}
        {/*</Text>*/}
      </View>
    );
    if (item.title === '' || !item.title) {
      return null;
    }
    return (
      <ChooseItem
        item={item}
        onPress={onChangeShippingMethod}
        active={selected && item.id === selected.get('id')}
        bottomElement={bottomElement}
        containerStyle={styles.containerItem}
        style={styles.item}
        key={item.id}
      />
    );
  };

  render() {
    const {cc, methodsNotCoveredByZone} = this.props;
    const {loading, data} = cc ? this.state : methodsNotCoveredByZone.toJS();
    const {shippingData} = this.state;
    return (
      <View style={styles.container}>
        {!loading ? (
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {shippingData &&
              shippingData.map((item, index) => this.renderItem({item, index}))}
          </ScrollView>
        ) : (
          <ActivityIndicator />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: margin.big,
  },
  item: {
    paddingVertical: padding.base,
  },
  containerItem: {
    marginRight: margin.base,
  },
  footerItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 1,
  },
});

ShippingMethod.defaultprops = {};

const mapStateToProps = state => ({
  methodsNotCoveredByZone: shippingMethodNotCoveredByZoneSelector(state),
  currency: currencySelector(state),
  defaultCurrency: defaultCurrencySelector(state),
  currencies: listCurrencySelector(state),
  language: languageSelector(state),
});

export default connect(mapStateToProps)(ShippingMethod);
