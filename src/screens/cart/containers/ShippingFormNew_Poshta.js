import React from 'react';
import {withTranslation} from 'react-i18next';
import {Map} from 'immutable';

import {View, StyleSheet} from 'react-native';
import {compose} from 'redux';
import {connect} from 'react-redux';

import {Row, Col} from 'src/containers/Gird';
import Input from 'src/containers/input/Input';
import InputCountry from './InputCountry';
import {
  languageSelector,
} from 'src/modules/common/selectors';

import {margin} from 'src/components/config/spacing';
import logger from '../../../utils/logger';

class ShippingFormNew_Poshta extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      region: [],
      city: [],
      numberPost: [],
      value: '',
      valueCity: '',
      valueNumber: '',
    }
  }

  componentDidMount() {
    const {language} = this.props
    logger.log('language123', language)
    fetch('https://api.novaposhta.ua/v2.0/json/', {
      method: 'POST',
      body: JSON.stringify({
        "modelName": "Address",
        "calledMethod": "getAreas",
        "methodProperties": {}
      })
    }).then(res => {
      return res.json()
    }).then(res => {
      const reg_data = res.data.map(value => {
        const name = language === 'ua' ? value.Description : value.DescriptionRu
        return {
          name,
          code: value.Ref,
          ...value,
        }
      })
      this.setState({ region: reg_data })
    }).catch(e => {
      logger.error('e', e)
    })
  }

  async onChangeReg(e, res) {
    const {language} = this.props
    fetch('https://api.novaposhta.ua/v2.0/json/', {
        method: 'POST',
        body: JSON.stringify({
          "modelName": "Address",
          "calledMethod": "getCities",
          "methodProperties": {
              "AreaRef": res.Ref
          }
        })
      }).then(res_json => {
        return res_json.json()
      }).then(res_json => {
        logger.log('res_json', res_json)
        const reg_data = res_json.data.map(res => {
          const name = language === 'ua' ? res.Description : res.DescriptionRu
          return {
            name,
            code: res.Ref,
            ...res,
          }
        })
        this.setState({ city: reg_data, value: res.name, valueCity: '', valueNumber: '' })
        this.props.onChange('address_post', res.name)
      }).catch(e => {
        logger.error('e', e)
      })
  }

  async onChangeCity (e, res) {
    const {language} = this.props
    fetch('https://api.novaposhta.ua/v2.0/json/', {
            method: 'POST',
            body: JSON.stringify({
              "modelName": "AddressGeneral",
              "calledMethod": "getWarehouses",
              "methodProperties": {
                  "CityName": res.Description
              }
          })
          }).then(res_json => {
            return res_json.json()
          }).then(res_json => {
            const reg_data = res_json.data.map(res => {
              const name = language === 'ua' ? res.Description : res.DescriptionRu
              return {
                name,
                code: res.Ref,
                ...res,
              }
            })
            logger.log('res_json', res)
            this.setState({ numberPost: reg_data, valueCity: res.name, valueNumber: '' })
            this.props.onChange('city', res.name)

          }).catch(e => {
            logger.error('e', e)
          })
  }


  async onChangeNumberPost (e, res) {
    const {value, valueCity} = this.state
    this.setState({ valueNumber: res.name })
    this.props.onChange('address_1', res.name)
  }

  render() {
    const {onChange, data, errors, t} = this.props;
    if (!data || !data.size) {
      return null;
    }
    logger.log('region', this.state.numberPost)
    return (
      <View>
        <Row style={styles.row}>
          <Col>
            <InputCountry
              error={errors && errors.get('region')}
              label={t('inputs:adress_region')}
              value={this.state.value}
              onChangeText={value => onChange('address_1', value)}
              onChange={(e, res) => this.onChangeReg(e, res)}
              country={{
                loading: false,
                data: this.state.region
              }}
            />
            <InputCountry
              error={errors && errors.get('region')}
              label={t('inputs:adress_city')}
              value={this.state.valueCity}
              // onChangeText={value => onChange('address_1', value)}
              onChange={(e, res) => this.onChangeCity(e, res)}
              country={{
                loading: false,
                data: this.state.city
              }}
              disabled={this.state.value === '' ? true : false}
            />
            <InputCountry
              error={errors && errors.get('region')}
              label={t('inputs:number_post')}
              value={this.state.valueNumber}
              // onChangeText={value => onChange('address_1', value)}
              onChange={(e, res) => this.onChangeNumberPost(e, res)}
              country={{
                loading: false,
                data: this.state.numberPost
              }}
              disabled={this.state.valueCity === '' ? true : false}
            />
            <Input
              error={errors && errors.get('note')}
              label={t('inputs:comment')}
              value={data.get('note')}
              onChangeText={value => onChange('note', value)}
            />
          </Col>
        </Row>
      </View>
    );
  }
}

ShippingFormNew_Poshta.defaultProps = {
  errors: Map(),
};

const styles = StyleSheet.create({
  row: {
    marginBottom: margin.base,
  },
});

const mapStateToProps = state => ({
  language: languageSelector(state),
});

export default compose(withTranslation(), connect(mapStateToProps))(ShippingFormNew_Poshta);
