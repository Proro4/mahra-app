import React from 'react';

import {compose} from 'redux';
import {connect} from 'react-redux';
import {fromJS, Map} from 'immutable';
import {withTranslation} from 'react-i18next';
import {showMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-community/async-storage';

import {ScrollView, StyleSheet, Switch, View} from 'react-native';
import {Text, ListItem} from 'src/components';
import Container from 'src/containers/Container';
import Button from 'src/containers/Button';
import {Row, Col} from 'src/containers/Gird';
import Heading from 'src/containers/Heading';
import ShippingForm from './ShippingForm';
import ShippingMethod from './ShippingMethod';
import {setOrderData} from 'src/modules/order/actions';
import RadioIcon from '../../shop/containers/RadioIcon';

import {shippingAddressSelector} from 'src/modules/auth/selectors';
import {
  selectShippingAddress,
  selectedShippingMethod,
} from 'src/modules/cart/selectors';
import {isLoginSelector, authSelector} from 'src/modules/auth/selectors';
import {changeData} from 'src/modules/cart/actions';
import {selectOrderData} from 'src/modules/order/selectors';
import {validatorAddress} from 'src/modules/cart/validator';
import {fetchCreateShippingMethod} from 'src/modules/cart/service';

import {margin, padding} from 'src/components/config/spacing';
import {calcCost} from 'src/utils/product';

import {red} from 'src/components/config/colors';
import logger from '../../../utils/logger';
import ShippingFormPlace from './ShippingFormPlace';
import ShippingFormNew_Poshta from './ShippingFormNew_Poshta';
import {languageSelector} from '../../../modules/common/selectors';

const payment_data = [
  {
    value: 'post',
    ua: 'Накладний платіж',
    ru: 'Наложенный платеж',
  },
  {
    value: 'cart',
    ua: 'Оплата карткою',
    ru: 'Оплата картой',
  },
];

class Shipping extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      useAsBilling: true,
      errors: Map(),
      isLoad: false,
      selected_payment: '',
      errorMessagePayment: '',
    };
  }

  componentDidMount = async () => {
    const {dispatch, user} = this.props;
    const first_name = await AsyncStorage.getItem('@first_name');
    const last_name = await AsyncStorage.getItem('@last_name');
    const email = await AsyncStorage.getItem('@email');
    const phone = await AsyncStorage.getItem('@phone');
    const user_name = await AsyncStorage.getItem('@user_name');
    const selected_payment_stor = await AsyncStorage.getItem(
      '@selected_payment',
    );

    if (selected_payment_stor) {
      this.setState({selected_payment: selected_payment_stor});
    }
    if (first_name) {
      dispatch(changeData(['shipping', 'first_name'], first_name));
    } else {
      dispatch(changeData(['shipping', 'first_name'], user.user?.firstname));
    }
    if (last_name) {
      dispatch(changeData(['shipping', 'last_name'], last_name));
    } else {
      dispatch(changeData(['shipping', 'last_name'], user.user?.lastname));
    }
    if (email) {
      dispatch(changeData(['shipping', 'email'], email));
    } else {
      dispatch(changeData(['shipping', 'email'], user.user?.email || ''));
    }
    if (phone) {
      dispatch(changeData(['shipping', 'phone'], phone));
    } else {
      dispatch(changeData(['shipping', 'phone'], user_name));
    }
  };

  onChange = async (key, value) => {
    const {dispatch} = this.props;
    dispatch(changeData(['shipping', key], value));
    AsyncStorage.setItem(`@${key}`, value);
  };

  /**
   * Handle re user my address
   */
  handleUserMyShippingAddress = () => {
    const {dispatch, shippingAddress} = this.props;
    dispatch(changeData(['shipping'], shippingAddress));
  };

  onChangeShippingMethod = method => {
    const {dispatch} = this.props;
    dispatch(
      changeData(
        ['shipping_lines', 0],
        fromJS({
          method_id: method.method_id,
          method_title: method.method_title,
          total: calcCost(method),
          ...method,
        }),
      ),
    );
  };

  handleNext = async () => {
    const {shipping, selected, nextStep, t, dispatch} = this.props;

    const selected_shippment = selected && selected.toJS();

    const data = shipping.toJS();

    if (selected_shippment && selected_shippment.id !== 'new_post') {
      AsyncStorage.setItem('@address', data.address_1);
    }

    const address =
      selected_shippment && selected_shippment.id === 'new_post'
        ? `Регіон: ${data.address_post}
Місто: ${data.city}
Номер відділення ${data.address_1}`
        : data.address_1;

    // Validation
    let errors = validatorAddress(shipping);

    if (this.state.selected_payment === '') {
      return this.setState({errorMessagePayment: t('common:text_err_payment')});
    } else {
      this.setState({errorMessagePayment: ''});
    }

    if (
      !selected ||
      (selected_shippment && selected_shippment.id === 'new_post'
        ? data.city === '' || data.address_1 === '' || data.address_post === ''
        : address === '')
    ) {
      errors = errors.set('shipping_lines', t('common:select_shipping_method'));
      return this.setState({errors});
    }
    this.setState({errors});

    if (
      Object.keys(errors.toJS()).length !== 0 &&
      errors.toJS().constructor !== Object
    ) {
      return false;
    }

    const product = this.props.orders.toJS().map(value => {
      return {
        product_id: value.id,
        quantity: value.quantity,
        characteristic_price_id: 0,
      };
    });

    logger.log('address', address)

    try {
      this.setState({isLoad: true});
      const order_data = {
        id: 1,
        subscribe: false,
        register: true,
        first_name: data.first_name,
        middle_name: '',
        last_name: data.last_name,
        email: data.email,
        phone: data.phone.replace(/ /g, ''),
        address,
        shipment: selected.toJS().id,
        payment: this.state.selected_payment === 'cart' ? 'card' : 'pay',
        comment: data.note,
      };

      logger.log('order_data', order_data)
      await AsyncStorage.setItem('@shipping_data', JSON.stringify(order_data));

      const res = await fetchCreateShippingMethod({
        autosave: false,
        cart_positions: product,
        order: order_data,
      });

      logger.log('res', res)

      this.setState({isLoad: false});
      this.props.dispatch(setOrderData([], true));
      const {useAsBilling} = this.state;
      if (useAsBilling) {
        dispatch(changeData(['billing'], shipping));
      }
      nextStep({useAsBilling, response: res});
    } catch (e) {
      showMessage({
        message: e.length > 0 ? e[0].message : t('validators:error'),
        type: 'danger',
        duration: 5000,
      });
      logger.log('e', e);
    } finally {
      this.setState({isLoad: false});
    }
  };

  onChoosePayment(value) {
    this.setState({selected_payment: value.value});
    AsyncStorage.setItem('@selected_payment', value.value);
  }

  render() {
    const {backStep, shipping, selected, language, t} = this.props;
    const {errors, errorMessagePayment} = this.state;
    const selected_shippment = selected && selected.toJS();

    return (
      <ScrollView>
        <Container style={styles.content}>
          <Heading
            title={t('cart:text_delivery')}
            // subTitleComponent={
            //   isLogin && (
            //     <Button
            //       title={t('cart:text_use_my_address')}
            //       type={'outline'}
            //       size={'small'}
            //       onPress={this.handleUserMyShippingAddress}
            //     />
            //   )
            // }
            containerStyle={[styles.textTitle, styles.headingDelivery]}
          />
          <ShippingForm
            errors={errors}
            data={shipping}
            onChange={this.onChange}
          />
          <Heading
            title={t('common:text_payment')}
            containerStyle={styles.textTitle}
          />
          {!!errorMessagePayment && (
            <Text style={StyleSheet.flatten([styles.error])}>
              {errorMessagePayment}
            </Text>
          )}
          {payment_data.map(value => (
            <ListItem
              key={value.value}
              title={language === 'ua' ? value.ua : value.ru}
              type="underline"
              small
              rightIcon={
                <RadioIcon
                  isSelect={this.state.selected_payment === value.value}
                />
              }
              containerStyle={styles.item}
              onPress={() => this.onChoosePayment(value)}
            />
          ))}
          {/* <View style={styles.useAsBilling}>
            <Text style={styles.usAsBillingText} colorSecondary>
              {t('cart:text_use_billing')}
            </Text>
            <Switch
              value={useAsBilling}
              onValueChange={() => this.setState({useAsBilling: !useAsBilling})}
            />
          </View> */}
          <Heading
            title={t('cart:text_shipping_method')}
            containerStyle={[styles.textTitle, {marginTop: 20}]}
          />

          <Text style={{color: red, fontSize: 10}}>
            {errors.get('shipping_lines')}
          </Text>

          <ShippingMethod
            selected={selected}
            onChangeShippingMethod={this.onChangeShippingMethod}
            // cc={shipping.get('country')}
          />
          {selected_shippment && selected_shippment.id === 'new_post' && (
            <ShippingFormNew_Poshta
              errors={errors}
              data={shipping}
              onChange={this.onChange}
            />
          )}
          {selected_shippment && selected_shippment.id !== 'new_post' && (
            <ShippingFormPlace
              errors={errors}
              data={shipping}
              onChange={this.onChange}
              e={this.onChange}
            />
          )}

          <Row style={styles.footer}>
            <Col>
              <Button
                type="outline"
                onPress={backStep}
                title={t('common:text_back')}
              />
            </Col>
            <Col>
              <Button
                onPress={this.handleNext}
                loading={this.state.isLoad}
                title={t('common:text_next')}
              />
            </Col>
          </Row>
        </Container>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  error: {
    fontSize: 10,
    color: '#FF0000',
  },
  content: {
    paddingTop: padding.large,
  },
  headingDelivery: {
    alignItems: 'center',
  },
  textTitle: {
    paddingTop: 0,
    paddingBottom: padding.base,
  },
  useAsBilling: {
    flexDirection: 'row',
    marginTop: margin.large + 4,
    marginBottom: margin.large + margin.big,
  },
  usAsBillingText: {
    marginVertical: 3,
    flex: 1,
  },
  footer: {
    marginBottom: margin.big,
  },
});

const mapStateToProps = state => ({
  shipping: selectShippingAddress(state),
  selected: selectedShippingMethod(state),
  shippingAddress: shippingAddressSelector(state),
  isLogin: isLoginSelector(state),
  orders: selectOrderData(state),
  language: languageSelector(state),
  user: authSelector(state),
});

export default compose(
  withTranslation(),
  connect(mapStateToProps),
)(Shipping);
