import React from 'react';

import {connect} from 'react-redux';

import AsyncStorage from '@react-native-community/async-storage';

import {View, I18nManager, Alert, Dimensions} from 'react-native';
import {SwipeListView} from 'react-native-swipe-list-view';
import {showMessage} from 'react-native-flash-message';
import {Header, ThemedView} from 'src/components';
import Container from 'src/containers/Container';
import Button from 'src/containers/Button';
import ButtonSwiper from 'src/containers/ButtonSwiper';
import {TextHeader, IconHeader} from 'src/containers/HeaderComponent';
import Empty from 'src/containers/Empty';
import CartItem from './containers/CartItem';
import CartTotal from './containers/CartTotal';
import Coupon from './containers/Coupon';

import {mainStack, homeTabs, authStack, rootSwitch, homeDrawer} from 'src/config/navigator';
import {margin} from 'src/components/config/spacing';
import {selectCartList, cartSizeSelector} from 'src/modules/cart/selectors';
import {
  selectOrderData,
  selectorPriceOrders,
} from 'src/modules/order/selectors';

import {removeCart} from 'src/modules/cart/actions';
import {
  removeOrderData,
  changeQuantity,
  setOrderData,
} from 'src/modules/order/actions';

import {
  wishListSelector,
  currencySelector,
  defaultCurrencySelector,
  configsSelector,
  getSiteConfig,
} from 'src/modules/common/selectors';
import {addWishList, removeWishList} from 'src/modules/common/actions';
import {checkQuantity} from 'src/utils/product';
import {isLoginSelector} from '../../modules/auth/selectors';
import logger from '../../utils/logger';

class CartScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      order_data: [],
      update: 0,
    };
  }
  goToProduct = product => {
    const {navigation} = this.props;
    navigation.navigate(mainStack.product, {product});
  };

  changeQuantity = (item, quantity) => {
    const {dispatch} = this.props;
    if (quantity > 0) {
      dispatch(changeQuantity(item, quantity));
    } else {
      dispatch(removeOrderData(item));
    }
  };

  getHandleWishList = product_id => {
    const {wishList, dispatch} = this.props;
    const hasList = wishList.has(product_id);
    const wishListAction = hasList
      ? () => dispatch(removeWishList(product_id))
      : () => dispatch(addWishList(product_id));
    return {
      type: hasList ? 'like' : 'unlike',
      onPress: wishListAction,
    };
  };

  removeOrderData = async (id, size) => {
    const {order_data, dispatch} = this.props;
    const data_storage = order_data.toJS().filter(value => {
      return value.id !== id || value.size !== size;
    });
    try {
      await AsyncStorage.setItem('@data_order', JSON.stringify(data_storage));
    } catch (e) {
      logger.log('e', e);
    }
    dispatch(setOrderData(data_storage));
  };

  componentDidMount() {
    this.setState({order_data: this.props?.order_data?.toJS()});
  }

  componentWillReceiveProps(nextProps) {
    // You don't have to do this check first, but it can help prevent an unneeded render
    logger.log('nextProps', nextProps?.order_data?.toJS());
    if (nextProps?.order_data?.toJS() !== this.state.order_data) {
      logger.log('123')
      this.setState({
        order_data: nextProps?.order_data?.toJS(),
        update: Math.random() * 1000,
      });
    }
  }

  renderData = () => {
    const {
      screenProps: {t},
      line_items,
      dispatch,
      navigation,
      currency,
      defaultCurrency,
      configs,
      siteConfig,
      isLogin,
      order_data,
    } = this.props;
    if (order_data.toJS().length <= 0) {
      return (
        <Empty
          icon="shopping-bag"
          title={t('empty:text_title_cart')}
          subTitle={t('empty:text_subtitle_cart')}
          clickButton={() => navigation.navigate(homeTabs.shop)}
        />
      );
    }
    const widthButton = configs.get('toggleWishlist') ? 140 : 70;
    const webviewCheckout = configs.get('webviewCheckout', true);
    const data = order_data.toJS();
    logger.log('state', this.state.order_data)
    return (
      <>
        <CartTotal style={styles.viewTotal} />
        <SwipeListView
          useFlatList
          removeClippedSubviews={false}
          keyExtractor={(item, index) => `${item && item.id}--${index}`}
          data={this.state.order_data || []}
          renderItem={({item, index}) => (
            <>
              {item === null && <View />}
              {item !== null && (
                <CartItem
                  currency={currency}
                  defaultCurrency={defaultCurrency}
                  index={index}
                  item={item}
                  t={t}
                  changeQuantity={this.changeQuantity}
                  goToProduct={this.goToProduct}
                  style={index === 0 && styles.firstItem}
                />
              )}
            </>
          )}
          renderHiddenItem={({item}) => (
            <View style={styles.viewButton}>
              {item === null && <View />}
              {item !== null && (
                <>
                  {configs.get('toggleWishlist') && (
                    <ButtonSwiper {...this.getHandleWishList(item.id)} />
                  )}
                  <ButtonSwiper
                    onPress={() => this.removeOrderData(item.id, item.size)}
                  />
                </>
              )}
            </View>
          )}
          leftOpenValue={widthButton}
          rightOpenValue={-widthButton}
          disableLeftSwipe={I18nManager.isRTL}
          disableRightSwipe={!I18nManager.isRTL}
          // ListFooterComponent={
          //   !webviewCheckout ? (
          //     <Container>
          //       <Coupon />
          //     </Container>
          //   ) : null
          // }
        />
        <Container style={styles.footerScrollview}>
          <Button
            title={t('cart:text_go_checkout')}
            onPress={() => {
              // const { priceOrders } = this.props
              // if (priceOrders < 50) {
              //   return Alert.alert(t('validators:50_uah'))
              // }
              if (!isLogin) {
                navigation.navigate(authStack.login);
              } else {
                navigation.navigate(mainStack.checkout);
              }
            }}
          />
        </Container>
      </>
    );
  };

  render() {
    const {
      screenProps: {t},
      order_data,
    } = this.props;
    const size = order_data.toJS().length;
    const subtitleHeader =
      size > 1
        ? t('common:text_items', {count: size})
        : t('common:text_item', {count: size});
    const {navigation} = this.props;
    return (
      <ThemedView isFullView>
        <Header
          leftComponent={<IconHeader />}
          centerComponent={
            <TextHeader
              title={t('common:text_cart')}
              subtitle={subtitleHeader}
            />
          }
          rightComponent={
            <IconHeader
                name="heart"
                size={24}
                onPress={() => navigation.navigate(homeTabs.wish_list)}
            />}
        />
        {this.renderData()}
      </ThemedView>
    );
  }
}

const styles = {
  viewTotal: {
    marginBottom: margin.large - 2,
  },
  firstItem: {
    borderTopWidth: 1,
  },
  viewButton: {
    width: '100%',
    height: 150,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  footerScrollview: {
    marginVertical: margin.large,
  },
};

CartScreen.defaultProps = {};

const mapStateToProps = state => {
  return {
    line_items: selectCartList(state),
    size: cartSizeSelector(state),
    wishList: wishListSelector(state),
    currency: currencySelector(state),
    defaultCurrency: defaultCurrencySelector(state),
    configs: configsSelector(state),
    siteConfig: getSiteConfig(state),
    isLogin: isLoginSelector(state),
    order_data: selectOrderData(state),
    priceOrders: selectorPriceOrders(state),
  };
};

export default connect(mapStateToProps)(CartScreen);
