import React from 'react';

import {connect} from 'react-redux';
import {DrawerActions} from 'react-navigation-drawer';
import {TestNotification} from '../components/testNotification'


import {home1} from 'src/mock/home-screen';

import {ScrollView, View, Text, Dimensions, Platform} from 'react-native';

import {ThemedView, Header} from 'src/components';
import {IconHeader, Logo, CartIcon} from 'src/containers/HeaderComponent';
import ModalHomePopup from 'src/containers/ModalHomePopup';

import {
  dataConfigSelector,
  toggleSidebarSelector,
} from 'src/modules/common/selectors';

// Containers
import Slideshow from './home/containers/Slideshow';
import ProductList from './home/containers/ProductList';
import ProductCategory from './home/containers/ProductCategory';
import CountDown from './home/containers/CountDown';
import BlogList from './home/containers/BlogList';
import Testimonials from './home/containers/Testimonials';
import logger from '../utils/logger';


const {width} = Dimensions.get('window');

const containers = {
  slideshow: Slideshow,
  products: ProductList,
  productcategory: ProductCategory,
  countdown: CountDown,
  blogs: BlogList,
  testimonials: Testimonials,
};

const widthComponent = spacing => {
  if (!spacing) {
    return width;
  }
  const marginLeft =
    spacing.marginLeft && parseInt(spacing.marginLeft, 10)
      ? parseInt(spacing.marginLeft, 10)
      : 0;
  const marginRight =
    spacing.marginRight && parseInt(spacing.marginRight, 10)
      ? parseInt(spacing.marginRight, 10)
      : 0;
  const paddingLeft =
    spacing.paddingLeft && parseInt(spacing.paddingLeft, 10)
      ? parseInt(spacing.paddingLeft, 10)
      : 0;
  const paddingRight =
    spacing.paddingRight && parseInt(spacing.paddingRight, 10)
      ? parseInt(spacing.paddingRight, 10)
      : 0;
  return width - marginLeft - marginRight - paddingLeft - paddingRight;
};

class HomeScreen extends React.Component {
  renderContainer(config) {
    const Container = containers[config.type];
    if (!Container) {
      return null;
    }
    return (
      <View key={config.id} style={config.spacing && config.spacing}>
        <Container
          {...config}
          widthComponent={widthComponent(config.spacing)}
        />
        <ProductList {...config} widthComponent={widthComponent()} />
      </View>
    );
  }

  componentDidMount() {
    const {navigation} = this.props;
    if (Platform.OS === 'ios') {
      navigation.dispatch(DrawerActions.openDrawer());
    }
  }

  render() {
    // const { category, product } = this.props;
    const {config, toggleSidebar, navigation} = this.props;

    return (
      <ThemedView isFullView>
        <Header
          leftComponent={
            toggleSidebar ? (
              <IconHeader
                name="align-left"
                size={22}
                onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
              />
            ) : null
          }
          centerComponent={<Logo />}
          rightComponent={<CartIcon />}
        />
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          {home1.map(conf => {
            return this.renderContainer(conf);
          })}


          {/* <View style={{opacity: 0, position: 'absolute'}}>
            <Slideshow />
          </View> */}
          <TestNotification />
          <ProductList
            {...home1[3]}
            fields={home1[3]}
            widthComponent={widthComponent()}
          />
          {/*<ProductList*/}
          {/*  {...home1[4]}*/}
          {/*  fields={home1[4]}*/}
          {/*  widthComponent={widthComponent()}*/}
          {/*/>*/}
          {/* <ProductCategory
            {...home1[3]}
            fields={home1[3]}
            widthComponent={widthComponent()}
          /> */}
          {/*<BlogList*/}
          {/*  {...home1[3]}*/}
          {/*  fields={home1[3]}*/}
          {/*  widthComponent={widthComponent()}*/}
          {/*/>*/}
        </ScrollView>
        <ModalHomePopup />
      </ThemedView>
    );
  }
}

const mapStateToProps = state => {
  return {
    config: dataConfigSelector(state),
    toggleSidebar: toggleSidebarSelector(state),
  };
};

export default connect(mapStateToProps)(HomeScreen);
