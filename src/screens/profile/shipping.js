import React, {Component} from 'react';

import {Header, ThemedView} from 'src/components';

import {IconHeader, CartIcon, TextHeader} from 'src/containers/HeaderComponent';
import ContainerTerm from './containers/ContainerTerm';
import ContainerShipping from './containers/ContainerShipping';

export default class ShippingScreen extends Component {
  static navigationOptions = {
    headerShown: false,
  };
  render() {
    const {
      screenProps: {t},
    } = this.props;
    return (
      <ThemedView isFullView>
        <Header
          leftComponent={<IconHeader />}
          centerComponent={<TextHeader title={t('cart:text_delivery')} />}
          rightComponent={<CartIcon />}
        />
        <ContainerShipping />
      </ThemedView>
    );
  }
}
