import React from 'react';
import {useTranslation} from 'react-i18next';
import {ScrollView, StyleSheet} from 'react-native';
import {Text} from 'src/components';
import Container from 'src/containers/Container';
import {margin} from 'src/components/config/spacing';
import {lineHeights} from 'src/components/config';
import {connect} from 'react-redux';
import {languageSelector} from 'src/modules/common/selectors';
import logger from '../../../utils/logger';

const ContainerShipping = ({language}) => {
  const {t} = useTranslation();
  if (language === 'ua') {
    return (
      <ScrollView>
        <Container>
          <Text h2 medium style={styles.title}>
            ДОСТАВКА ТОВАРІВ З ОПТОВОГО МАГАЗИНУ РУШНИКІВ МАХРОВИЙ СВІТ
          </Text>
          <Text medium style={styles.title}>
            {`Ви вже вибрали для себе рушник, постільну білизну або кілька товарів відразу? Якщо ви ще не визначилися з покупкою, скористайтеся нашим каталогом. Якщо вам потрібна допомога, зателефонуйте нам по телефону: 068 0-322-322 і наші фахівці обов'язково вам допоможуть.

Ваше замовлення буде доставлений однією з наступних кур'єрських служб

Нова Пошта,
Гюнсел,
Інтайм,
Делівері,
Автолюкс,
Укрпошта.
Є можливість самовивозу`}
          </Text>
          <Text h4 center bold style={styles.center}>
            Замовлення на суму менше ніж 1000 грн, наложеним платежем не
            відправляємо
          </Text>
          <Text h4 center medium style={styles.title}>
            {`
Ви можете забрати замовлення у нас зі складу самостійно, для цього уточніть дату і час у нашого менеджера, він підготує ваш товар заздалегідь. 

Важливо! При заповненні форми замовлення ОБОВ'ЯЗКОВО вказуйте свій контактний телефон і e-mail. Інакше як ми зможемо відправити вам товар?
    
Також ви можете замовити вподобані товари за нашими телефонами.

+38(068) 0-322-322   +38(095) 211-77-22

     `}
          </Text>
        </Container>
      </ScrollView>
    );
  }
  return (
    <ScrollView>
      <Container>
        <Text h2 medium style={styles.title}>
          ДОСТАВКА ТОВАРОВ ИЗ ОПТОВОГО МАГАЗИНА ПОЛОТЕНЕЦ МАХРОВЫЙ МИР
        </Text>
        <Text medium style={styles.title}>
          {`Вы уже выбрали для себя полотенце, постельное белье или несколько товаров сразу? Если вы еще не определились с покупкой, воспользуйтесь нашим каталогом. Если вам нужна помощь – позвоните нам по телефону: 068 0-322-322 и наши специалисты обязательно вам помогут.

Ваш заказ будет доставлен одной из следующих курьерских служб

Новая Почта,
Гюнсел,
Интайм,
Деливери,
Автолюкс,
Укрпочта.
`}
        </Text>
        <Text h4 center bold style={styles.center}>
          Заказы на сумму менее чем на 1000 грн, наложенным платежом не
          отправляем
        </Text>
        <Text h4 center medium style={styles.title}>
          {`
Вы можете забрать заказ у нас со склада самостоятельно, для этого уточните дату и время у нашего менеджера, он подготовит ваш товар заранее. 

Важно! При заполнении формы заказа ОБЯЗАТЕЛЬНО указывайте свой контактный телефон и e-mail. Иначе как мы сможем отправить вам товар?

Также вы можете заказать понравившиеся товары по нашим телефонам.
+38(068) 0-322-322   +38(095) 211-77-22
ВНИМАНИЕ!
Обработка заказа занимает до 2-х дней.
 `}
        </Text>
      </Container>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  title: {
    marginBottom: margin.big,
  },
  titleList: {
    marginBottom: margin.base + 4,
  },
  description: {
    marginBottom: 50,
    lineHeight: lineHeights.h4,
  },
  center: {
    textAlign: 'center',
  },
});

const mapStateToProps = state => {
  return {
    language: languageSelector(state),
  };
};

export default connect(mapStateToProps)(ContainerShipping);
