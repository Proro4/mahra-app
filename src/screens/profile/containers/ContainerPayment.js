import React from 'react';
import {useTranslation} from 'react-i18next';
import {ScrollView, StyleSheet} from 'react-native';
import {Text} from 'src/components';
import Container from 'src/containers/Container';
import {margin} from 'src/components/config/spacing';
import {lineHeights} from 'src/components/config';
import {connect} from 'react-redux';
import {languageSelector} from 'src/modules/common/selectors';
import logger from '../../../utils/logger';

const ContainerPayment = ({language}) => {
  const {t} = useTranslation();
  if (language === 'ua') {
    return (
      <ScrollView>
        <Container>
          <Text h2 medium style={styles.title}>
            ОПЛАТА
          </Text>
          <Text h4 bold style={styles.title}>
            Мы принимаем все известные способы оплаты, а именно:
          </Text>
          <Text medium style={styles.title}>
            {`Ви вже вибрали для себе рушник, постільну білизну або кілька товарів відразу? Якщо ви ще не визначилися з покупкою, скористайтеся нашим каталогом. Якщо вам потрібна допомога - зателефонуйте нам за телефоном: 068 0-322-322 і наші фахівці обов'язково вам допоможуть.

Визначилися? Саме час оформити замовлення.

    Виберіть сподобалися моделі з тих, що є в наявності.
    Збережіть посилання на сторінки.
    Напишіть нам листа і вкажіть товари, які ви хотіли б купити.
    Надсилайте лист і не забудьте вказати ваш номер телефону.
    Чекайте дзвінка від нашого менеджера.

Важливо! При заповненні форми замовлення ОБОВ'ЯЗКОВО вказуйте свій контактний телефон і e-mail. Інакше як ми зможемо відправити вам товар?

Розміри рушників, пледів та простирадел можуть відрізнятися від тих що представлені на сайті від 5 до 10 сантиметрів в залежності від партії товару.

Також ви можете замовити вподобані товари за нашими телефонами.

(0382) 77-77-16;

(068) 0-322-322;

Увага! Обробка замовлення займає до 2-х днів.`}
          </Text>
        </Container>
      </ScrollView>
    );
  }
  return (
    <ScrollView>
      <Container>
        <Text h2 medium style={styles.title}>
        ОПЛАТА
        </Text>
        <Text h4 bold style={styles.title}>
        Мы принимаем все известные способы оплаты, а именно:
        </Text>
        <Text medium style={styles.title}>
          {`Оплата наличными в г. Хмельницком;
Перевод на карточку ПриватБанк;
Пополнение карточного счета другого банка;
Почтовый или банковский перевод;
`}
        </Text>
        <Text h4 bold style={styles.title}>
        Что выбрать?
        </Text>
        <Text medium style={styles.title}>
          {`Если вы работаете с нами впервые - рекомендуем вам выбрать оплату наличными при получении. Это удобно, выгодно и надежно. Вы не вносите предоплату, а потому не несете никакого риска. Только после того, как вы получили и проверили комплектацию заказа, вы его оплачиваете.
`}
        </Text>
        <Text h4 bold style={styles.title}>
        Другие варианты оплаты:

        </Text>
        <Text medium style={styles.title}>
          {`Возможна предоплата наличным или безналичным переводом.

Если вам удобнее оплатить товар банковским переводом, то при оформлении заказа вам будут предоставлены реквизиты для оплаты счета.
`}
        </Text>
      </Container>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  title: {
    marginBottom: margin.big,
  },
  titleList: {
    marginBottom: margin.base + 4,
  },
  description: {
    marginBottom: 50,
    lineHeight: lineHeights.h4,
  },
  center: {
    textAlign: 'center',
  },
});

const mapStateToProps = state => {
  return {
    language: languageSelector(state),
  };
};

export default connect(mapStateToProps)(ContainerPayment);
