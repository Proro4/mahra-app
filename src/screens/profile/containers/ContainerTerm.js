import React from 'react';
import {useTranslation} from 'react-i18next';
import {ScrollView, StyleSheet} from 'react-native';
import {Text} from 'src/components';
import Container from 'src/containers/Container';
import {margin} from 'src/components/config/spacing';
import {lineHeights} from 'src/components/config';
import {connect} from 'react-redux';
import {languageSelector} from 'src/modules/common/selectors';
import logger from '../../../utils/logger';

const ContainerTerm = ({language}) => {
  const {t} = useTranslation();
  if (language === 'ua') {
    return (
      <ScrollView>
        <Container>
          <Text h2 medium style={styles.title}>
            КАК ЗАКАЗАТЬ?
          </Text>
          <Text h4 bold style={styles.title}>
            Як зробити замовлення і зв'язатися з нами?
          </Text>
          <Text medium style={styles.title}>
            {`Ви вже вибрали для себе рушник, постільну білизну або кілька товарів відразу? Якщо ви ще не визначилися з покупкою, скористайтеся нашим каталогом. Якщо вам потрібна допомога - зателефонуйте нам за телефоном: 068 0-322-322 і наші фахівці обов'язково вам допоможуть.

Визначилися? Саме час оформити замовлення.

    Виберіть сподобалися моделі з тих, що є в наявності.
    Збережіть посилання на сторінки.
    Напишіть нам листа і вкажіть товари, які ви хотіли б купити.
    Надсилайте лист і не забудьте вказати ваш номер телефону.
    Чекайте дзвінка від нашого менеджера.

Важливо! При заповненні форми замовлення ОБОВ'ЯЗКОВО вказуйте свій контактний телефон і e-mail. Інакше як ми зможемо відправити вам товар?

Розміри рушників, пледів та простирадел можуть відрізнятися від тих що представлені на сайті від 5 до 10 сантиметрів в залежності від партії товару.

Також ви можете замовити вподобані товари за нашими телефонами.

(0382) 77-77-16;

(068) 0-322-322;

Увага! Обробка замовлення займає до 2-х днів.`}
          </Text>
        </Container>
      </ScrollView>
    );
  }
  return (
    <ScrollView>
      <Container>
        <Text h2 medium style={styles.title}>
          КАК ЗАКАЗАТЬ?
        </Text>
        <Text h4 bold style={styles.title}>
          Как сделать заказ и связаться с нами?
        </Text>
        <Text medium style={styles.title}>
          {`Вы уже выбрали для себя полотенце, постельное белье или несколько товаров сразу? Если вы еще не определились с покупкой, воспользуйтесь нашим каталогом. Если вам нужна помощь – позвоните нам по телефону: 068 0-322-322 и наши специалисты обязательно вам помогут.

Определились? Самое время оформить заказ.

    Выберите понравившиеся модели из тех, что есть в наличии.
    Сохраните ссылки на странички.
    Напишите нам письмо и укажите товары, которые вы хотели бы купить.
    Отправляйте письмо и не забудьте указать ваш номер телефона.
    Ожидайте звонка от нашего менеджера.        

Важно! При заполнении формы заказа ОБЯЗАТЕЛЬНО указывайте свой контактный телефон и e-mail. Иначе как мы сможем отправить вам товар?

Размеры полотенец, пледов и простыней могут отличаться от тех что представлены на сайте от 5 до 10 сантиметров в зависимости от партии товара.

Также вы можете заказать понравившиеся товары по нашим телефонам.

(0382) 77-77-16;

(068) 0-322-322;

Внимание! Обработка заказа занимает до 2-х дней. `}
        </Text>
      </Container>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  title: {
    marginBottom: margin.big,
  },
  titleList: {
    marginBottom: margin.base + 4,
  },
  description: {
    marginBottom: 50,
    lineHeight: lineHeights.h4,
  },
  center: {
    textAlign: 'center',
  },
});

const mapStateToProps = state => {
  return {
    language: languageSelector(state),
  };
};

export default connect(mapStateToProps)(ContainerTerm);
