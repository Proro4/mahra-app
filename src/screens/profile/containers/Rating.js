import React from 'react';
import {Text, ListItem} from 'src/components';
import {StyleSheet} from 'react-native';
import {margin, padding} from 'src/components/config/spacing';
import {grey4} from 'src/components/config/colors';
import {useTranslation} from 'react-i18next';
import {icon, titleProps} from './config';
import Rate, {AndroidMarket} from 'react-native-rate';
import options from 'src/config/config-rate';

const RatingContainer = ({clickPage}) => {
  const {t} = useTranslation();
  return (
    <>
      <Text medium style={styles.title}>
        {t('common:rating')}
      </Text>
      <ListItem
        leftIcon={icon(1, 'info')}
        title={t('profile:text_rate_app')}
        type="underline"
        titleProps={titleProps}
        chevron
        pad={padding.large}
        containerStyle={styles.itemEnd}
        // onPress={() => {
        //   Rate.rate(options, success => {
        //     if (success) {
        //       // this technically only tells us if the user successfully went to the Review Page. Whether they actually did anything, we do not know.
        //       this.setState({rated: true});
        //     }
        //   });
        // }}
      />
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    color: grey4,
    marginTop: margin.big + 4,
    marginBottom: margin.small,
  },
  itemEnd: {
    borderBottomWidth: 0,
  },
});

export default RatingContainer;
