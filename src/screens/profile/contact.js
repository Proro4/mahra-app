import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  Image,
  ImageBackground,
  Dimensions,
  Linking,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon, Text, ThemedView} from 'src/components';

import {IconHeader} from 'src/containers/HeaderComponent';

import {configsSelector, languageSelector} from 'src/modules/common/selectors';

import {grey5} from 'src/components/config/colors';
import {margin, padding, borderRadius} from 'src/components/config/spacing';

import instagram from 'src/assets/images/instagram.png';
import viber from 'src/assets/images/viber.png';

const {width} = Dimensions.get('window');

class ContactScreen extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };
  onCall(phoneNumber) {
    Linking.openURL(`tel:${phoneNumber}`);
  }

  onLink(url) {
    Linking.openURL(`${url}`);
  }

  render() {
    const {
      screenProps: {theme, t},
      language,
    } = this.props;
    return (
      <ThemedView isFullView colorSecondary>
        <ImageBackground
          source={require('src/assets/images/mapUkraine.jpg')}
          style={styles.imagebg}
          resizeMode="cover">
          <Image
            source={require('src/assets/images/mapUkraine.jpg')}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              width: '100%',
              height: '71%',
            }}
            resizeMode="cover"
          />
          <Header
            leftComponent={<IconHeader />}
            containerStyle={styles.header}
          />
          <View style={styles.content(theme.Modal.backgroundColor)}>
            <Text h2 medium style={styles.title}>
              {t('profile:text_name_contact')}
            </Text>
            <View style={styles.row}>
              <View
                style={[
                  styles.flex,
                  styles.marginRight('small'),
                  styles.listInfo,
                ]}>
                <View style={[styles.viewInfo, styles.row]}>
                  <Icon
                    name="mail"
                    type="feather"
                    size={15}
                    color={grey5}
                    containerStyle={styles.marginRight('small')}
                  />
                  <Text>yurii.vavryniuk@gmail.com</Text>
                </View>
                <View
                  style={[
                    styles.viewInfo,
                    styles.row,
                    {justifyContent: 'space-between'},
                  ]}>
                  <View style={[styles.row, {alignItems: 'center'}]}>
                    <Icon
                      name="phone-call"
                      type="feather"
                      size={15}
                      color={grey5}
                      containerStyle={styles.marginRight('small')}
                    />
                    <View>
                      <Text h4 onPress={() => this.onCall('+38 068 0-322-322')}>
                        +38 068 0-322-322
                      </Text>
                      <Text h4 onPress={() => this.onCall('+38 068 0-322-322')}>
                        +38(095) 211-77-22
                      </Text>
                    </View>
                  </View>
                  <View>
                    <Text>/ Оля, Юрій</Text>
                  </View>
                </View>
                <View style={[styles.viewInfo, styles.row]}>
                  <Icon
                    name="clock"
                    type="feather"
                    size={15}
                    color={grey5}
                    containerStyle={styles.marginRight('small')}
                  />
                  {language === 'ua' && (
                    <View>
                      <Text>ПН-ПТ - 9:00 - 20:00</Text>
                      <Text>Субота 9:00-15:00</Text>
                      <Text>Неділя вихідний</Text>
                    </View>
                  )}
                  {language !== 'ua' && (
                    <View>
                      <Text>ПН-ПТ - 9:00 - 20:00</Text>
                      <Text>Суббота 9:00-15:00</Text>
                      <Text>Воскресенье выходной</Text>
                    </View>
                  )}
                </View>
                <View style={[styles.viewInfo, styles.row]}>
                  <Icon
                    name="map"
                    type="feather"
                    size={15}
                    color={grey5}
                    containerStyle={styles.marginRight('small')}
                  />
                  {language === 'ua' && (
                    <Text>
                      м.Хмельницький, Львівське шосе, ринок Дарсон, кіоск 92/1.
                    </Text>
                  )}
                  {language !== 'ua' && (
                    <Text>
                      г.Хмельницкий, Львовское шоссе, рынок Дарсон, киоск 92/1.
                    </Text>
                  )}
                </View>
                <View style={styles.row}>
                  <TouchableOpacity
                    onPress={() =>
                      this.onLink('viber://chat?number=+380680322322')
                    }>
                    <Image
                      source={viber}
                      style={[
                        styles.paddingRight('base'),
                        {width: 25, height: 25},
                      ]}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.onLink('https://www.facebook.com/mahra.com.ua/')
                    }>
                    <Icon
                      name="facebook"
                      type="font-awesome"
                      size={25}
                      color="#4867AA"
                      containerStyle={styles.paddingRight('base')}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      this.onLink('https://instagram.com/makhrovyy_mir')
                    }>
                    <Image
                      source={instagram}
                      style={[
                        styles.paddingRight('base'),
                        {width: 25, height: 25},
                      ]}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <Icon
                      name="telegram"
                      type="font-awesome"
                      size={25}
                      color="#4867AA"
                      containerStyle={styles.paddingRight('base')}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <Image
                source={require('src/assets/images/maps-and-flags.png')}
                resizeMode="stretch"
              />
            </View>
          </View>
        </ImageBackground>
      </ThemedView>
    );
  }
}

const styles = {
  imagebg: {
    width,
    height: '100%',
    justifyContent: 'space-between',
  },
  header: {
    backgroundColor: 'transparent',
  },
  content: bgColor => ({
    backgroundColor: bgColor,
    paddingVertical: padding.big,
    paddingHorizontal: padding.big,
    borderTopLeftRadius: borderRadius.big,
    borderTopRightRadius: borderRadius.big,
  }),
  title: {
    marginBottom: margin.big - margin.base,
  },
  listInfo: {
    flex: 1,
    marginTop: margin.base,
  },
  row: {
    flexDirection: 'row',
  },
  marginRight: size => ({
    marginRight: margin[size],
  }),
  paddingRight: size => ({
    marginRight: padding[size],
  }),
  viewInfo: {
    alignItems: 'center',
    marginBottom: margin.base,
  },
};

const mapStateToProps = state => {
  return {
    configs: configsSelector(state),
    language: languageSelector(state),
  };
};

export default connect(mapStateToProps)(ContactScreen);
