import React, {Component} from 'react';

import {Header, ThemedView} from 'src/components';

import {IconHeader, CartIcon, TextHeader} from 'src/containers/HeaderComponent';
import ContainerPayment from './containers/ContainerPayment';

export default class PaymentScreen extends Component {
  static navigationOptions = {
    headerShown: false,
  };
  render() {
    const {
      screenProps: {t},
    } = this.props;
    return (
      <ThemedView isFullView>
        <Header
          leftComponent={<IconHeader />}
          centerComponent={<TextHeader title={t('common:text_payment')} />}
          rightComponent={<CartIcon />}
        />
        <ContainerPayment />
      </ThemedView>
    );
  }
}
