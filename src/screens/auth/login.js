import React from 'react';
import {connect} from 'react-redux';

import {
  StyleSheet,
  ScrollView,
  View,
  KeyboardAvoidingView,
  Linking,
  Platform,
} from 'react-native';
import InputMobile from 'src/containers/input/InputMobile';
import {Header, Divider, Text, ThemedView} from 'src/components';
import Container from 'src/containers/Container';
import Input from 'src/containers/input/Input';
import Button from 'src/containers/Button';
import TextHtml from 'src/containers/TextHtml';
import {TextHeader, IconHeader} from 'src/containers/HeaderComponent';
import SocialMethods from './containers/SocialMethods';
import {showMessage} from 'react-native-flash-message';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';

import {rootSwitch, authStack} from 'src/config/navigator';

import {signInWithEmail, setUserData} from 'src/modules/auth/actions';
import {authSelector} from 'src/modules/auth/selectors';
import {loginWithEmail, getProfileData} from 'src/modules/auth/service';
import authSaga from 'src/modules/auth/saga';

import {requiredLoginSelector} from 'src/modules/common/selectors';
import {margin} from 'src/components/config/spacing';

import {changeColor} from 'src/utils/text-html';
import AsyncStorage from '@react-native-community/async-storage';

class LoginScreen extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      username: '+38',
      password: '',
    };
  }

  initGoogleAuth = async () => {
    try {
      GoogleSignin.configure({
        webClientId:
          '704614556834-p9f5m5urrstmhl9c5klamrqivdmfnu5i.apps.googleusercontent.com',
      });
    } catch (e) {
      console.log('Google auth init', e);
    }
  };

  initUsername = async () => {
    try {
      const user_name = await AsyncStorage.getItem('@user_name');
      if (user_name && user_name !== '') {
        this.setState({username: user_name});
      }
    } catch {}
  };

  componentDidMount() {
    this.initGoogleAuth();
    this.initUsername();
  }

  handleLogin = async () => {
    const {
      screenProps: {t},
    } = this.props;

    const {username, password} = this.state;
    try {
      const res = await loginWithEmail({username, password});
      await AsyncStorage.setItem('@token', res.token);
      await AsyncStorage.setItem('@user_name', username);
      const getData = await getProfileData();
      this.props.dispatch(setUserData(getData));
      this.props.navigation.navigate(rootSwitch.main);
    } catch (e) {
      showMessage({
        message: t('auth:something_went_wrong'),
        type: 'danger',
      });
      console.log('error', e);
    }
    // this.props.dispatch(signInWithEmail({username, password}));
  };

  googleSignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const isSignIn = await GoogleSignin.isSignedIn();

      if (isSignIn) {
        await GoogleSignin.signOut();
      }

      const oserInfo = await GoogleSignin.signIn();
      console.log('userInfo', oserInfo);
    } catch (e) {
      console.log('GOOGLE SIGN Full', e);
      // if (e.code === statusCodes.SIGN_IN_CANCELLED) {
      //   return console.log('GOOGLE SIGN  SIGN_IN_CANCELLED', error);
      // } else if (e.code === statusCodes.IN_PROGRESS) {
      //   console.log('GOOGLE SIGN  IN_PROGRESS', error);
      // } else if (e.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      //   console.log('GOOGLE SIGN  PLAY_SERVICES_NOT_AVAILABLE', error);
      // } else {
      // }
    }
  };

  render() {
    const {
      navigation,
      auth: {pending, loginError},
      requiredLogin,
      screenProps: {t, theme},
    } = this.props;
    const {username, password} = this.state;
    const {message, errors} = loginError;

    return (
      <ThemedView isFullView>
        <Header
          leftComponent={
            !requiredLogin && (
              <IconHeader
                name="x"
                size={24}
                onPress={() => navigation.navigate(rootSwitch.main)}
              />
            )
          }
          centerComponent={<TextHeader title={t('common:text_signin')} />}
        />
        <ScrollView
          keyboardShouldPersistTaps="handled"
          removeClippedSubviews={false}>
          <KeyboardAvoidingView behavior="height" style={styles.keyboard}>
            <Container>
              {message ? (
                <TextHtml
                  value={message}
                  style={changeColor(theme.colors.error)}
                />
              ) : null}
              <InputMobile
                label={`${t('profile:text_input_phone')} *`}
                value={username}
                onChangeText={value => this.setState({username: value})}
                error={errors && errors.username}
              />
              <Input
                label={t('auth:text_input_password')}
                value={password}
                secureTextEntry
                onChangeText={value => this.setState({password: value})}
                error={errors && errors.password}
              />
              <Button
                title={t('common:text_signin')}
                loading={pending}
                onPress={this.handleLogin}
                containerStyle={styles.margin}
              />
              <Text
                onPress={() => navigation.navigate(authStack.forgot)}
                style={styles.textForgot}
                medium>
                {t('auth:text_forgot')}
              </Text>
              {/* <View style={[styles.viewOr, styles.margin]}>
                <Divider style={styles.divOr} />
                <Text style={styles.textOr} colorThird>
                  {t('auth:text_or')}
                </Text>
                <Divider style={styles.divOr} />
              </View> */}
              {/* <SocialMethods style={styles.viewSocial} /> */}
            </Container>
          </KeyboardAvoidingView>
        </ScrollView>
        <Container style={styles.margin}>
          {/* <Text h6 colorThird style={styles.textAccount}>
            {t('auth:text_have_account')}
          </Text> */}
          <Button
            title={t('auth:text_register')}
            type="outline"
            onPress={() => navigation.navigate(authStack.register)}
          />
        </Container>
      </ThemedView>
    );
  }
}

const styles = StyleSheet.create({
  keyboard: {
    flex: 1,
  },
  textForgot: {
    textAlign: 'center',
  },
  viewOr: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  divOr: {
    flex: 1,
  },
  textOr: {
    marginHorizontal: margin.base,
  },
  textAccount: {
    textAlign: 'center',
    marginBottom: margin.base,
  },
  margin: {
    marginVertical: margin.big,
  },
  viewSocial: {
    marginBottom: margin.big,
  },
});

const mapStateToProps = state => {
  return {
    auth: authSelector(state),
    requiredLogin: requiredLoginSelector(state),
  };
};

export default connect(mapStateToProps)(LoginScreen);
