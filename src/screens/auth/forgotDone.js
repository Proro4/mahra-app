import React from 'react';
import {Text, View} from 'react-native';
import DoneChangePass from './containers/DoneChangePass';

const ForgotDone = () => {
  return <DoneChangePass />;
};

export default ForgotDone;
