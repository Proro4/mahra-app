import React from 'react';
import {compose} from 'redux';
import {withNavigation} from 'react-navigation';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';

import {StyleSheet, View} from 'react-native';
import {withTheme, Text, Avatar} from 'src/components';

import Container from 'src/containers/Container';
import Button from 'src/containers/Button';

import {authStack} from 'src/config/navigator';

import {white} from 'src/components/config/colors';
import {margin} from 'src/components/config/spacing';
import {lineHeights} from 'src/components/config/fonts';
import {languageSelector} from '../../../modules/common/selectors';

class DoneChangePass extends React.Component {
  handleContinue = () => {
    const {navigation} = this.props;
    navigation.navigate(authStack.login);
  };

  render() {
    const {theme, t} = this.props;
    return (
      <Container style={styles.container}>
        <View style={styles.content}>
          <Avatar
            rounded
            icon={{
              name: 'check',
              size: 47,
              color: white,
            }}
            size={95}
            overlayContainerStyle={{
              backgroundColor: theme.colors && theme.colors.success,
            }}
            containerStyle={styles.icon}
          />
          <Text h2 medium style={styles.textTitle}>
            {t('cart:text_congrats')}
          </Text>
          <Text h4 medium style={styles.textDescription}>
            {t('auth:success_forgot')}
          </Text>
        </View>
        <Button
          title={t('common:text_back')}
          onPress={this.handleContinue}
          containerStyle={styles.button}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    marginBottom: margin.big + 4,
  },
  textTitle: {
    marginBottom: margin.base,
  },
  textDescription: {
    textAlign: 'center',
    lineHeight: lineHeights.h4,
  },
  button: {
    marginVertical: margin.big,
  },
});

const mapStateToProps = state => {
  return {
    language: languageSelector(state),
  };
};

export default compose(
  withTranslation(),
  withNavigation,
  withTheme,
  connect(mapStateToProps),
)(DoneChangePass);
