import React, {Component} from 'react';
import {Text, View} from 'react-native';

import {Map} from 'immutable';
import take from 'lodash/take';
import isEqual from 'lodash/isEqual';
import split from 'lodash/split';

import {withNavigation} from 'react-navigation';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {compose} from 'recompose';

import {mainStack} from 'src/config/navigator';
import {productListTypes} from 'src/config/product';

import Container from 'src/containers/Container';
import Heading from 'src/containers/Heading';
import Products from '../Products';

import {
  currencySelector,
  defaultCurrencySelector,
  languageSelector,
  daysBeforeNewProductSelector,
} from 'src/modules/common/selectors';
import {getProducts, topSellers} from 'src/modules/product/service';
import {prepareProductItem} from 'src/utils/product';
import {handleError} from 'src/utils/error';
import logger from '../../../../utils/logger';

const initHeader = {
  style: {},
};

const getType = fields => {
  if (!fields) {
    return productListTypes.latest;
  }
  const type =
    fields.product_type && fields.product_type.type
      ? fields.product_type.type
      : fields.type
      ? fields.type
      : productListTypes.latest;
  return type;
};

const getInclude = fields => {
  if (!fields) {
    return [];
  }
  const ids =
    fields.product_type &&
    fields.product_type.type === productListTypes.custom &&
    fields.product_type.ids
      ? fields.product_type.ids
      : '';
  return split(ids, ',');
};

class ProductList extends Component {
  constructor(props, context) {
    super(props, context);
    const type = getType(props.fields);
    const per_page =
      props.fields && props.fields.limit ? props.fields.limit : 4;
    const include = getInclude(props.fields);

    this.state = {
      data: [],
      loading: false,
      per_page,
      type,
      include: include,
    };
  }
  //
  componentDidMount() {
    this.fetchData();
    // this.getFilter();
  }

  getFilter = async (type = this.state.type) => {
    logger.log('getFAILTER', productListTypes);
    logger.log('type', type);
    try {
      let results = this.state.include;
      if (type === productListTypes.best_sales) {
        const topSellerProducts = await topSellers({
          period: 'year',
        });
        if (topSellerProducts.length) {
          results = topSellerProducts.map(({product_id}) => product_id);
        }
      }
      logger.log('result', results);

      this.setState(
        {
          include: results,
        },
        this.fetchData,
      );
    } catch (e) {
      this.fetchData();
    }
  };

  fetchData = () => {
    const {language} = this.props;
    this.setState(
      {
        loading: true,
      },
      async () => {
        try {
          logger.log('language', language)
          let data;
          if (this.props.fields.type === 'latest') {
            const query = {
              'per-page': 6,
              updated_at: true,
              expand:
                'categories,images,thumbnails,characteristics,out_of_stock_status,characteristic_prices,url',
              lang: language,
              'filter[category_id]': 9,
              'filter[status]': 1,
            };
            data = await getProducts(query);
          } else {
            const query = {
              'per-page': 6,
              updated_at: true,
              expand:
                'categories,images,thumbnails,characteristics,out_of_stock_status,characteristic_prices,url',
              lang: language,
              'filter[is_action]': 1,
              // 'filter[status]': 1,
            };
            data = await getProducts(query);
          }
          logger.log('productList', data);

          this.setState({
            data,
            loading: false,
          });
        } catch (error) {
          this.setState({
            loading: false,
          });
          handleError(error);
        }
      },
    );

    // if (type === productListTypes.best_sales) {
    //   this.getBestSeller(per_page);
    // } else {
    //   this.getLatest(per_page);
    // }
  };
  componentDidUpdate(prevProps) {
    if (!isEqual(prevProps.fields, this.props.fields)) {
      const per_page =
        this.props.fields && this.props.fields.limit
          ? this.props.fields.limit
          : 4;

      const type = getType(this.props.fields);
      const include = getInclude(this.props.fields);

      this.setState(
        {
          per_page,
          type,
          include,
        },
        () => this.getFilter(type, per_page),
      );
    }
  }
  /**
   * Prepare product item to render on FlatList
   * @param item
   * @returns {*}
   */
  prepareProduct = item => {
    const {currency, defaultCurrency, days} = this.props;
    const mapItem = Map(item);
    const result = prepareProductItem(mapItem, currency, defaultCurrency, days);
    return result.toJS();
  };

  render() {
    const {
      navigation,
      navigationType,
      headingElement,
      layout,
      fields,
      widthComponent,
      t,
    } = this.props;
    const {data, include} = this.state;

    if (
      !fields ||
      typeof fields !== 'object' ||
      Object.keys(fields).length < 1
    ) {
      return null;
    }

    const heading = fields.text_heading ? fields.text_heading : initHeader;

    const listData = data.map(this.prepareProduct);
    const listDataFilter = listData.filter(value => value.status !== 0);

    const headerDisable = !fields.boxed ? 'all' : 'none';

    return (
      <>
        {listDataFilter?.length > 0 && (
          <View style={{marginTop: 40}}>
            {this.props.fields && (
              <Container disable={headerDisable}>
                {headingElement ? (
                  headingElement
                ) : (
                  <Heading
                    title={
                      this.props.fields.type === 'latest'
                        ? t('common:text_new')
                        : t('common:text_promotional')
                    }
                    style={heading.style}
                    containerStyle={{paddingTop: 0}}
                    onPress={() =>
                      navigation.navigate(mainStack.products, {
                        name:
                          this.props.fields.type === 'latest'
                            ? t('common:text_new')
                            : t('common:text_promotional'),
                        filterBy: Map({
                          include: include,
                        }),
                        id: this.props.fields.type === 'latest' ? 9 : null,
                      })
                    }
                    subTitle={t('common:text_show_all')}
                  />
                )}
              </Container>
            )}
            <Products
              data={listDataFilter}
              layout={layout}
              fields={fields}
              widthComponent={widthComponent}
              navigationType={navigationType}
              type="latest"
            />
          </View>
        )}
      </>
    );
  }
}

ProductList.defaultProps = {
  headingElement: null,
  // layout: productListLayout.slide,
};

const mapStateToProps = state => ({
  currency: currencySelector(state),
  defaultCurrency: defaultCurrencySelector(state),
  language: languageSelector(state),
  days: daysBeforeNewProductSelector(state),
});

export default compose(
  withTranslation(),
  withNavigation,
  connect(mapStateToProps),
)(ProductList);
