import React, {Component} from 'react';

import {connect} from 'react-redux';
import {mainStack} from 'src/config/navigator';
import {withNavigation} from 'react-navigation';
import {compose} from 'recompose';

import {
  StyleSheet,
  View,
  Dimensions,
  ImageBackground,
  Platform,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {Text, Avatar, Image, withTheme} from 'src/components';
import Container from 'src/containers/Container';
import Pagination from 'src/containers/Pagination';

import {languageSelector} from 'src/modules/common/selectors';

import {padding} from 'src/components/config/spacing';

const {width} = Dimensions.get('window');

class SlideshowBasic extends Component {
  state = {
    pagination: 0,
  };

  render() {
    const {theme, language, fields, widthComponent, clickGoPage} = this.props;
    const {pagination} = this.state;

    // if (
    //   !fields ||
    //   typeof fields !== 'object' ||
    //   Object.keys(fields).length < 1
    // ) {
    //   return null;
    // }

    if (!fields) {
      return null;
    }

    const widthImage = Dimensions.get('window').width;
    const heightImage = 350;
    const images = fields.thumbnails ? fields.thumbnails : [];

    const widthView = Dimensions.get('window').width;
    const heightView = (widthView * heightImage) / widthImage;
    const heightFooter = 25;
    const heightScroll = heightView + heightFooter;

    const autoplayDelay =
      fields.auto_play_delay && parseInt(fields.auto_play_delay)
        ? parseInt(fields.auto_play_delay)
        : 1000;
    const autoplayInterval =
      fields.auto_play_interval && parseInt(fields.auto_play_interval)
        ? parseInt(fields.auto_play_interval)
        : 1800;

    const styleImage = {
      width: widthView,
      height: heightView,
      justifyContent: 'flex-end',
    };

    return (
      <Container>
        <Carousel
          data={fields}
          renderItem={({item}) => {
            return (
              <View
                style={{height: heightScroll}}
                // key={i} // we will use i for the key because no two (or more) elements in an array will have the same index
              >
                {/*... we will return a square Image with the corresponding object as the source*/}
                <Image
                  ImageComponent={ImageBackground}
                  style={styleImage}
                  imageStyle={styles.viewImage}
                  source={
                    item.thumbnails && item.thumbnails[0]
                      ? {uri: item.thumbnails[0].url}
                      : require('src/assets/images/pDefault.png')
                  }>
                  <View style={styles.viewInfo}>
                    {item.heading && (
                      <Text medium style={item.heading.style}>
                        {item.heading.text && item.heading.text[language]}
                      </Text>
                    )}
                    <Text light style={[styles.textTitle]}>
                      {/* {`${item.name} `} */}
                      {item.seo_title && (
                        <Text medium style={[styles.textTitle]}>
                          {item.name}
                        </Text>
                      )}
                    </Text>
                  </View>
                </Image>
                <Avatar
                  icon={{
                    name: 'arrow-right',
                    size: 22,
                    color: theme.colors.bgColor,
                    isRotateRTL: true,
                  }}
                  rounded
                  size={50}
                  containerStyle={styles.buttonNext}
                  overlayContainerStyle={{
                    backgroundColor: theme.colors.primary,
                  }}
                  onPress={() => {
                    console.log('product', item);
                    this.props.navigation.navigate(mainStack.product, {
                      product: item,
                    })
                  }}
                />
              </View>
            );
          }}
          sliderWidth={widthView}
          itemWidth={widthView}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          autoplayDelay={autoplayDelay}
          autoplayInterval={5000}
          loop
          autoplay={true}
          onSnapToItem={index => this.setState({pagination: index})}
        />
        {fields.length && (
          <Pagination
            activeVisit={pagination}
            count={fields.length}
            containerStyle={styles.viewPagination}
          />
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  viewImage: {
    resizeMode: 'stretch',
  },
  viewInfo: {
    paddingHorizontal: padding.base,
    paddingVertical: padding.big,
  },
  textTitle: {
    fontSize: 35,
    color: '#F1711D',
    lineHeight: 38,
  },
  viewPagination: {
    position: 'absolute',
    bottom: 0,
    left: padding.large,
    justifyContent: 'flex-start',
    marginRight: 50 + padding.large,
  },
  buttonNext: {
    position: 'absolute',
    bottom: 0,
    right: 30,
    ...Platform.select({
      android: {
        elevation: 4,
      },
    }),
  },
});

const mapStateToProps = state => ({
  language: languageSelector(state),
});

SlideshowBasic.defaultProps = {
  widthComponent: width,
};

export default compose(
  connect(mapStateToProps),
  withNavigation,
  withTheme,
)(SlideshowBasic);
