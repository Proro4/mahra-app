import React from 'react';
import {Dimensions, View, Text} from 'react-native';
import SlideshowBasic from './Basic';
import SlideshowCreative from './Creative';
import {getProducts} from 'src/modules/product/service';
import {connect} from 'react-redux';
import {languageSelector} from '../../../../modules/common/selectors';

import action from 'src/utils/action';
import logger from '../../../../utils/logger';

const {width} = Dimensions.get('window');

class Slideshow extends React.Component {
  constructor(props) {
    super();
    this.state = {
      fields: [],
    };
  }
  async componentDidMount() {
    const query = {
      'per-page': 6,
      expand:
        'categories,images,thumbnails,characteristics,out_of_stock_status,url',
      lang: this.props.language,
      'filter[category_id]': 9,
    };
    try {
      const res = await getProducts(query);
      logger.log('res', res);
      this.setState({fields: res});
    } catch {
      logger.log('err get product');
    }
  }
  render() {
    const {layout, widthComponent} = this.props;
    const {fields} = this.state;

    if (
      !fields ||
      typeof fields !== 'object' ||
      Object.keys(fields).length < 1
    ) {
      return null;
    }
    if (layout === 'creative') {
      return (
        <SlideshowCreative
          fields={fields.items}
          widthComponent={widthComponent}
          clickGoPage={data => action(data)}
        />
      );
    }

    return (
      <View>
        <SlideshowBasic
          fields={fields}
          widthComponent={widthComponent}
          clickGoPage={data => action(data)}
        />
      </View>
    );
  }
}

Slideshow.defaultProps = {
  widthComponent: width,
};
const mapStateToProps = state => {
  return {
    language: languageSelector(state),
  };
};

export default connect(mapStateToProps)(Slideshow);
