import React from 'react';
import {connect} from 'react-redux';

import {
  StyleSheet,
  View,
  ActivityIndicator,
  I18nManager,
  RefreshControl,
} from 'react-native';
import {Header, ThemedView} from 'src/components';
import {SwipeListView} from 'react-native-swipe-list-view';
import ProductItem from 'src/containers/ProductItem';
import ButtonSwiper from 'src/containers/ButtonSwiper';
import {TextHeader, CartIcon, IconHeader} from 'src/containers/HeaderComponent';
import Empty from 'src/containers/Empty';

import {removeWishList} from 'src/modules/common/actions';
import {fetchWishList} from 'src/modules/product/actions';
import {
  loadingWishListSelector,
  dataWishListSelector,
} from 'src/modules/product/selectors';

import {wishListSelector, languageSelector} from 'src/modules/common/selectors';
import {getProducts} from 'src/modules/product/service';

import {margin} from 'src/components/config/spacing';
import {homeTabs} from 'src/config/navigator';
import logger from '../utils/logger';

class WishListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      refreshing: false,
    };
  }

  // componentDidUpdate(prevProps) {
  //   logger.log('prevProps', prevProps);
  //   // logger.log('currentProps', nextProps);
  //   // if ([...nextProps.wishList].length !== this.state.data.length) {
  //   //   this.fetchDataList();
  //   // }
  //   // return true;
  // }
  componentDidMount() {
    this.fetchDataList();
  }

  fetchDataList = async () => {
    const {language, wishList} = this.props;
    let queryFilter = {};
    wishList.toJS().forEach((value, index) => {
      const params = `filter[or][0][id][in][${index}]`;
      queryFilter = {...queryFilter, [params]: value};
    });

    if (wishList.toJS().length === 0) {
      this.setState({data: []});
      return;
    }
    const query = {
      expand:
        'categories,images,thumbnails,characteristics,out_of_stock_status,characteristic_prices,url',
      lang: language,
      ...queryFilter,
    };
    this.setState({refreshing: true});
    try {
      const data = await getProducts(query);

      this.setState({data});
    } catch {
      logger.error('cant get products');
      //
    } finally {
      this.setState({refreshing: false});
    }
  };

  fetchData = (data = this.props.wishList) => {
    const {dispatch} = this.props;
    dispatch(fetchWishList(data.toJS()));
  };

  removeItem = product_id => {
    const {data} = this.state;
    const {dispatch} = this.props;
    dispatch(removeWishList(product_id));
    const boof = data.filter(value => {
      if (value.id !== product_id) {
        return value;
      }
    });
    this.setState({data: boof});
  };

  componentDidUpdate(prevProps) {
    const {wishList} = this.props;
    logger.log('prevProps.wishList', prevProps.wishList.size, wishList.size);
    if (wishList.size !== prevProps.wishList.size) {
      this.fetchDataList();
    }
  }
  renderData = () => {
    const {
      screenProps: {t},
      navigation,
      wishList,
    } = this.props;

    const {data, refreshing} = this.state;
    if (!data || data.length < 1) {
      return (
        <Empty
          icon="heart"
          title={t('empty:text_title_wishlist')}
          subTitle={t('empty:text_subtitle_wishlist')}
          titleButton={t('common:text_go_shopping')}
          clickButton={() => navigation.navigate(homeTabs.shop)}
        />
      );
    }

    return (
      <SwipeListView
        useFlatList
        keyExtractor={item => `${item.id}`}
        data={data}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={this.fetchDataList}
          />
        }
        wishList={wishList.toJS()}
        renderItem={({item, index}) => (
          <ProductItem
            item={item}
            style={index === 0 ? styles.firstItem : undefined}
            type="wishlist"
          />
        )}
        renderHiddenItem={({item}) => (
          <View style={styles.viewSwiper}>
            <ButtonSwiper
              onPress={() => {
                this.removeItem(item.id);
              }}
            />
          </View>
        )}
        leftOpenValue={70}
        rightOpenValue={-70}
        disableLeftSwipe={I18nManager.isRTL}
        disableRightSwipe={!I18nManager.isRTL}
      />
    );
  };

  render() {
    const {
      wishList,
      loading,
      screenProps: {t},
    } = this.props;

    const {data} = this.state;

    const subtitle =
      data.length > 1
        ? t('common:text_items', {count: data.length})
        : t('common:text_item', {count: data.length});

    return (
      <ThemedView style={styles.container}>
        <Header
          leftComponent={<IconHeader />}
          centerComponent={
            <TextHeader title={t('common:text_wishList')} subtitle={subtitle} />
          }
          rightComponent={<CartIcon />}
        />
        {loading ? (
          <View style={styles.viewLoading}>
            <ActivityIndicator size="small" />
          </View>
        ) : (
          this.renderData()
        )}
      </ThemedView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewLoading: {
    marginVertical: margin.large,
  },
  viewSwiper: {
    height: '100%',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  firstItem: {
    borderTopWidth: 1,
  },
});

const mapStateToProps = state => ({
  data: dataWishListSelector(state),
  loading: loadingWishListSelector(state),
  wishList: wishListSelector(state),
  language: languageSelector(state),
});

export default connect(mapStateToProps)(WishListScreen);
