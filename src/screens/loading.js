// @flow

import React, {Component} from 'react';
import {Platform} from 'react-native';
import {NavigationScreenProps} from 'react-navigation';

// import isArray from 'lodash/isArray';
// import isObject from 'lodash/isObject';

import {connect} from 'react-redux';
import {fetchSettingSuccess} from 'src/modules/common/actions';
import {isGettingStartSelector} from 'src/modules/common/selectors';
import {isLoginSelector} from 'src/modules/auth/selectors';
import {fetchCategories} from 'src/modules/category/actions';
import {rootSwitch} from 'src/config/navigator';
import {clearCartAddress} from 'src/modules/cart/actions';

import {
  fetchSetting,
  fetchConfig,
  fetchTemplate,
} from 'src/modules/common/service';

import SplashScreen from 'react-native-splash-screen';
import logger from '../utils/logger';

type Props = {
  initSetting: Function,
  navigation: NavigationScreenProps,
};

class LoadingScreen extends Component<Props> {
  componentDidMount() {
    this.bootstrapAsync();
  }

  /**
   * Init data
   * @returns {Promise<void>}
   */
  bootstrapAsync = async () => {
    try {
      const {
        initSetting,
        fetchCategories,
        isGettingStart,
        clearCartAddress,
        isLogin,
        navigation,
      } = this.props;
      console.log('isGettingStart', isGettingStart)
      clearCartAddress();
      // Fetch setting
      let settings = await fetchSetting();
      // logger.log('settings', settings)
      const configs = await fetchConfig();
      // const templates = await fetchTemplate();

      const {...rest} = settings;
      initSetting({
        settings: rest,
        configs: configs,
        templates: [
          {
            id: '1',
            name: 'New Template',
            data:
              '[{"type":"banners","name":"Banners","icon":"file-image","layout":{"value":"three","options":[{"key":"one","name":"One Column"},{"key":"two","name":"Two Columns"},{"key":"three","name":"Three Columns"},{"key":"multiplecolumn","name":"Multiple Columns"},{"key":"carousel","name":"Carousel"}]},"fields":[{"key":"boxed","field":"switch","value":false,"defaultValue":false,"name":"Boxed"},{"key":"disable_heading","field":"switch","value":false,"defaultValue":false,"name":"Disable heading"},{"key":"text_heading","field":"text","value":{"text":{"en":"Banners"},"style":{}},"defaultValue":{},"name":"Text heading"},{"key":"pad","field":"input","value":0,"name":"Pad"},{"key":"width","field":"input","value":375,"name":"Width"},{"key":"height","field":"input","value":390,"name":"Height"},{"key":"radius","field":"input","value":0,"name":"Border radius"},{"key":"images","field":"items","items":[{"fieldKey":"image","name":"Image","field":"image"},{"fieldKey":"action","name":"Action","field":"action"},{"fieldKey":"title","name":"Title","field":"text"}],"value":[{"id":"836d041e-8f23-498f-a0e4-5fa3be4906a1","active":true,"title":{"text":{"en":"Category"},"style":{"color":"#d0021b","fontSize":30}}},{"id":"4bf97847-f116-4d3d-a8b3-4ab73d197d9b","active":false,"title":{"text":{"en":"11111"},"style":{}}}],"name":"Image"}],"spacing":[{"key":"padding","field":"spacing","name":"Padding","value":{"paddingTop":0,"paddingRight":0,"paddingBottom":0,"paddingLeft":0}},{"key":"margin","field":"spacing","name":"Margin","value":{"marginTop":0,"marginRight":0,"marginBottom":0,"marginLeft":0}}],"id":"7b78e81a-264e-449f-b823-71b789be866b"}]',
            settings:
              '[{"type":"app_config","name":"App config","fields":[{"key":"logo","field":"image","value":{},"name":"Logo"},{"key":"layout_category","field":"select","options":[{"name":"Style 1","value":"category1"},{"name":"Style 2","value":"category2"},{"name":"Style 3","value":"category3"},{"name":"Style 4","value":"category4"}],"value":"","defaultValue":"category1","name":"Layout Category"},{"key":"text_category","field":"text","value":{"text":{"en":"Up to 40% Off Holiday Bit"},"style":{}},"defaultValue":{},"name":"Category Text"}]},{"type":"colors","name":"Colors","fields":[{"key":"primary","field":"color","value":"#121212","name":"Primary Color"},{"key":"secondary","field":"color","value":"#777777","name":"Secondary Color"},{"key":"bgColor","field":"color","value":"#ffffff","name":"Background Color"},{"key":"bgColorSecondary","field":"color","value":"#f4f4f4","name":"Background Secondary Color"}]},{"type":"popup","name":"Popup","fields":[{"key":"enable","field":"switch","value":false,"name":"Enable popup"},{"key":"heading","field":"text","value":{"text":{"en":""},"style":{}},"defaultValue":{},"name":"Heading"},{"key":"title","field":"text","value":{"text":{"en":""},"style":{}},"defaultValue":{},"name":"Title"},{"key":"description","field":"text","value":{"text":{"en":""},"style":{}},"defaultValue":{},"name":"Description"},{"key":"text_button","field":"text","value":{"text":{"en":""},"style":{}},"defaultValue":{},"name":"Button Name"},{"key":"action_button","field":"action","name":"Action Button"}]}]',
            status: '0',
            date_created: '2020-05-12 08:34:50',
            date_updated: '2020-05-12 08:34:50',
          },
          {
            id: '2',
            name: 'Design 1',
            data: '[{"type":"slideshow","name":"Slideshow","icon":"pl"',
            settings:
              '[{"type":"app_config","name":"App config","fields":[{"key":"logo","field":"image","value":{},"name":"Logo"},{"key":"layout_category","field":"select","options":[{"name":"Style 1","value":"category1"},{"name":"Style 2","value":"category2"},{"name":"Style 3","value":"category3"}],"value":"category1","defaultValue":"category1","name":"Layout Category"},{"key":"text_category","field":"text","value":{"text":{"en":"Up to 40% Off Holiday Bit"},"style":{}},"defaultValue":{},"name":"Category Text"}]},{"type":"colors","name":"Colors","fields":[{"key":"primary","field":"color","value":"#121212","name":"Primary Color"},{"key":"secondary","field":"color","value":"#777777","name":"Secondary Color"},{"key":"bgColor","field":"color","value":"#ffffff","name":"Background Color"},{"key":"bgColorSecondary","field":"color","value":"#f4f4f4","name":"Background Secondary Color"}]},{"type":"popup","name":"Popup","fields":[{"key":"enable","field":"switch","value":false,"name":"Enable popup"},{"key":"heading","field":"text","value":{"text":{"en":""},"style":{}},"defaultValue":{},"name":"Heading"},{"key":"title","field":"text","value":{"text":{"en":""},"style":{}},"defaultValue":{},"name":"Title"},{"key":"description","field":"text","value":{"text":{"en":""},"style":{}},"defaultValue":{},"name":"Description"},{"key":"text_button","field":"text","value":{"text":{"en":""},"style":{}},"defaultValue":{},"name":"Button Name"},{"key":"action_button","field":"action","name":"Action Button"}]}]',
            status: '0',
            date_created: '2020-08-04 04:20:22',
            date_updated: '2020-08-04 04:20:22',
          },
        ],
      });
      // Fetch categories
      fetchCategories();
      const router = isGettingStart
        ? rootSwitch.start
        : false && !isLogin
        ? rootSwitch.auth
        : rootSwitch.main;
      navigation.navigate(router);
      SplashScreen.hide();
    } catch (e) {
      console.error(e);
    }
  };

  render() {
    return null;
  }
}

const mapStateToProps = state => {
  return {
    isGettingStart: isGettingStartSelector(state),
    isLogin: isLoginSelector(state),
  };
};

const mapDispatchToProps = {
  initSetting: fetchSettingSuccess,
  fetchCategories: fetchCategories,
  clearCartAddress: clearCartAddress,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoadingScreen);
