import React, {useMemo} from 'react';
import {StyleSheet, ScrollView, TouchableOpacity, View} from 'react-native';
import {Text, withTheme} from 'src/components';
import {margin, padding, borderRadius} from 'src/components/config/spacing';
import {exclude_categories} from 'src/config/category';
import {excludeCategory} from 'src/utils/category';

const CategoryList = ({onPress, data: data, theme, select}) => {
  if (!data || data.length === 0) {
    return null;
  }

  const sortCatalog = useMemo(() => {
    const dataFilter = excludeCategory(data, exclude_categories);
    return dataFilter
      .map(c => {
        return c;
      })
      .sort((a, b) => b?.sort - a?.sort);
  }, [data]);
  return (
    <View>
      <ScrollView
        horizontal={true}
        // pagingEnabled={true} // animates ScrollView to nearest multiple of it's own width
        showsHorizontalScrollIndicator={false}
        style={styles.container}>
        {sortCatalog.map((item, index) => {
          if (item.parent_categories?.length > 0) {
            return null;
          }
          let boofer = [];
          let isSimple = false;
          if (item.child_categories && item.child_categories.length > 0) {
            data.reverse().forEach(value => {
              if (item.id === value.parent_id) {
                boofer.push(value);
              }
              if (item.name === value.name) {
                isSimple = true;
              }
              // c.child_categories.forEach(value_c => {
              //   if (value_c === value.id) {
              //     boofer.push(value);
              //     return value;
              //   }
              // });
            });

            boofer.sort((a, b) =>
              b?.sort === 0 && a.sort === 0 ? a.id - b.id : b?.sort - a?.sort,
            );
          }
          return (
            <>
              {!isSimple && (
                <TouchableOpacity
                  key={`${item.key}_${index}`}
                  onPress={() => onPress(item)}
                  style={[
                    styles.item,
                    index === 0 && styles.itemFirst,
                    index === sortCatalog.length - 1 && styles.itemLast,
                    {borderColor: theme.CategoryProductList.borderColor},
                  ]}>
                  <Text
                    h6
                    style={[
                      styles.textName,
                      {color: theme.CategoryProductList.color},
                    ]}>
                    {item.name}
                  </Text>
                </TouchableOpacity>
              )}
              {boofer.map((value_boof, index_value_boof) => (
                <TouchableOpacity
                  key={`${value_boof.key}_${index_value_boof}`}
                  onPress={() => onPress(value_boof)}
                  style={[
                    styles.item,
                    index_value_boof === 0 && styles.itemFirst,
                    index_value_boof === sortCatalog.length - 1 && styles.itemLast,
                    {borderColor: theme.CategoryProductList.borderColor},
                    // select === value.key && styles.activeSort,
                  ]}>
                  <Text
                    h6
                    style={[
                      styles.textName,
                      {color: theme.CategoryProductList.color},
                    ]}>
                    {value_boof.name}
                  </Text>
                </TouchableOpacity>
              ))}
            </>
          );
        })}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 38,
    marginBottom: margin.big,
  },
  item: {
    borderWidth: 1,
    borderRadius: borderRadius.base,
    justifyContent: 'center',
    paddingHorizontal: padding.big,
    marginRight: margin.small,
  },
  itemFirst: {
    marginLeft: margin.large,
  },
  itemLast: {
    marginRight: margin.large,
  },
  textName: {
    lineHeight: 17,
  },
  activeSort: {
    borderColor: '#000',
  },
});

CategoryList.defaultProps = {
  onPress: () => {},
  data: [],
};

export default withTheme(CategoryList);
