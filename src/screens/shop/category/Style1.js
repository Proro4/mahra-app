import React, {useMemo} from 'react';
import {connect} from 'react-redux';
import {StyleSheet, FlatList} from 'react-native';
import {ListItem} from 'src/components';
import Container from 'src/containers/Container';
import EmptyCategory from './EmptyCategory';
import {categorySelector} from 'src/modules/category/selectors';
import {borderRadius, margin, padding} from 'src/components/config/spacing';
import {grey6} from 'src/components/config/colors';
import unescape from 'lodash/unescape';
import {excludeCategory} from 'src/utils/category';
import {exclude_categories} from 'src/config/category';
import logger from '../../../utils/logger';

const noImage = require('src/assets/images/imgCateDefault.png');

const Style1 = ({category, goProducts}) => {
  const data = excludeCategory(category.data, exclude_categories);
  const sortCatalog = useMemo(() => {
    return data
      .map(c => {
        return c;
      })
      .sort((a, b) => b?.sort - a?.sort);
  }, [data]);

  return (
    <>
      {data.length < 1 ? (
        <EmptyCategory />
      ) : (
        <Container style={styles.content}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            keyExtractor={item => `${item.id}`}
            data={sortCatalog}
            renderItem={({item}) => {
              if (item.parent_categories?.length > 0) {
                return null;
              }
              let boofer = [];
              let isSimple = false;
              if (item.child_categories && item.child_categories.length > 0) {
                data.reverse().forEach(value => {
                  if (item.id === value.parent_id) {
                    boofer.push(value);
                  }
                  if (item.name === value.name) {
                    isSimple = true;
                  }
                });

                boofer.sort((a, b) =>
                  b?.sort === 0 && a.sort === 0
                    ? a.id - b.id
                    : b?.sort - a?.sort,
                );
              }
              return (
                <>
                  {!isSimple && (
                    <ListItem
                      title={unescape(item.name)}
                      titleProps={{
                        h4: true,
                      }}
                      leftAvatar={{
                        rounded: true,
                        source: item.thumbnail
                          ? {
                              uri: item.thumbnail,
                              cache: 'reload',
                            }
                          : noImage,
                        size: 60,
                      }}
                      chevron
                      onPress={() => goProducts(item, sortCatalog)}
                      style={styles.item}
                      containerStyle={{paddingVertical: padding.base}}
                    />
                  )}
                  {boofer.map((value_boof, index) => (
                    <ListItem
                      title={unescape(value_boof.name)}
                      titleProps={{
                        h4: true,
                      }}
                      leftAvatar={{
                        rounded: true,
                        source: value_boof.thumbnail
                          ? {
                              uri: value_boof.thumbnail,
                              cache: 'reload',
                            }
                          : noImage,
                        size: 60,
                      }}
                      key={`${value_boof.name}_${index}`}
                      chevron
                      onPress={() => goProducts(value_boof)}
                      style={styles.item}
                      containerStyle={{paddingVertical: padding.base}}
                    />
                  ))}
                </>
              );
            }}
          />
        </Container>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  notification: {
    marginBottom: margin.base,
    marginTop: margin.large,
  },
  content: {
    flex: 1,
  },
  item: {
    marginBottom: margin.base,
  },
  badge: {
    minWidth: 18,
    height: 18,
    borderRadius: borderRadius.base + 1,
  },
  textBadge: {
    lineHeight: 18,
    color: grey6,
  },
});

const mapStateToProps = state => {
  return {
    category: categorySelector(state),
  };
};

export default connect(mapStateToProps)(Style1);
