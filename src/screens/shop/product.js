/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';

import {compose} from 'recompose';
import {fromJS, List, Map} from 'immutable';
import {connect} from 'react-redux';
import axios from 'axios';
import merge from 'lodash/merge';
import AsyncStorage from '@react-native-community/async-storage';

import {showMessage} from 'react-native-flash-message';
import {StyleSheet, View, Dimensions} from 'react-native';
import {Text, ThemedView} from 'src/components';
import Price from 'src/containers/Price';
import Container from 'src/containers/Container';
import Empty from 'src/containers/Empty';
import TextHtml from 'src/containers/TextHtml';

import ScrollProductDetail from './product/ScrollProductDetail';
import RelatedProducts from './containers/RelatedProducts';
import ProductVariable from './product/ProductVariable';
import ProductExternal from './product/ProductExternal';
import ProductGrouped from './product/ProductGrouped';
import ProductImages from './product/ProductImages';
import ProductStock from './product/ProductStock';
import Button from 'src/components/buttons/Button';

import {updateOrderData} from 'src/modules/order/actions';
import {
  attributeSelector,
  dataRatingSelector,
} from 'src/modules/product/selectors';
import {
  defaultCurrencySelector,
  currencySelector,
  languageSelector,
  configsSelector,
} from 'src/modules/common/selectors';
import {selectOrderData} from 'src/modules/order/selectors';
import CounterPrice from './containers/CounterPrice';

import {prepareProductItem} from 'src/utils/product';
import {changeColor, changeSize} from 'src/utils/text-html';

import {getSingleData, defaultPropsData} from 'src/hoc/single-data';
import {withLoading} from 'src/hoc/loading';

import {homeTabs} from 'src/config/navigator';
import {margin} from 'src/components/config/spacing';
import * as productType from 'src/config/product';

import {detailVendorSelector} from 'src/modules/vendor/selectors';
import logger from '../../utils/logger';

const {width} = Dimensions.get('window');
const HEADER_MAX_HEIGHT = width;

class Product extends Component {
  static navigationOptions = {
    headerShown: false,
  };

  constructor(props, context) {
    super(props, context);

    const {navigation, data, currency, defaultCurrency} = props;
    const product = navigation.getParam('product', {});

    // no need get days in prepareProductItem
    const dataPrepare = prepareProductItem(
      fromJS(data),
      currency,
      defaultCurrency,
    );
    const dataProduct = product && product.id ? fromJS(product) : dataPrepare;
    this.state = {
      product: product,
      images: product.thumbnails,
      loadingVariation: dataProduct.get('type') === productType.VARIABLE, // Loading state fetch product variations
      quantity: 0,
      variation: Map(),
      meta_data: List(),
      variations: List(),
      isAddToCart: false,
      current_price: 0,
      current_checked: 0,
      priceList: {},
    };
  }

  async getData() {
    logger.log('123', JSON.parse(await AsyncStorage.getItem('@data_order')));
  }

  componentDidMount() {
    this.getData();

    const {product} = this.state;
    logger.log('product', product);
    if (
      product &&
      product.characteristic_prices &&
      product.characteristic_prices.length > 0 &&
      product.characteristic_prices[0].price
    ) {
      this.setState({current_price: product.characteristic_prices[0].price});
    } else {
      this.setState({current_price: product.price});
    }
    const arr = {};
    product &&
      product.characteristic_prices &&
      product.characteristic_prices.length > 0 &&
      product.characteristic_prices.map((value, index) => {
        arr[value.id.toString()] = 0;
      });
    this.setState({priceList: arr});
  }

  componentWillUnmount() {
    if (this.source) {
      this.source.cancel('User exist the screen.');
    }
  }

  addToCart = async () => {
    const {
      screenProps: {t, theme},
    } = this.props;
    if(this.state.quantity > 0){

      const {product, quantity, variation, meta_data, priceList} = this.state;
      const {dispatch, order_data} = this.props;

      const listProduct = Object.values(priceList);


      if (listProduct?.length === 0) {
        const obj = {
          ...product,
          product_id: product.id,
          price: product.price * product.buy_step,
          fix_price: product.price,
          quantity: product.buy_step,
          size: '',
        };
        dispatch(updateOrderData(obj, () => this.setState({isAddToCart: true})));
        const data_async = [...order_data.toJS()];
        let isHas = false;

        data_async.map(value => {
          if (value.product_id === obj.product_id && value.size === obj.size) {
            isHas = true;
            return obj;
          }
          return value;
        });
        if (!isHas) {
          data_async.push(obj);
        }
        try {
          await AsyncStorage.setItem('@data_order', JSON.stringify(data_async));
        } catch (e) {
          logger.log('async storage err', e);
        }
        return showMessage({
          message: t('empty:text_product_add_store'),
          type: 'success',
        });
      }

      await listProduct.map(async value => {
        if (value && value.price && value.price !== 0) {
          const obj = {
            ...product,
            product_id: product.id,
            price: value.price,
            fix_price: value.fix_price,
            quantity: value.count,
            size: value.size,
            // variation,
            // product,
            // meta_data,
          };
          dispatch(
            updateOrderData(obj, () => this.setState({isAddToCart: true})),
          );
          const data_async = [...order_data.toJS()];
          let isHas = false;
          data_async.map(value => {
            if (value.product_id === obj.product_id && value.size === obj.size) {
              isHas = true;
              return obj;
            }
            return value;
          });
          if (!isHas) {
            data_async.push(obj);
          }
          try {
            logger.log('data_async', data_async);
            await AsyncStorage.setItem('@data_order', JSON.stringify(data_async));
          } catch (e) {
            logger.log('async storage err', e);
          }
          showMessage({
            message: t('empty:text_product_add_store'),
            type: 'success',
          });
        }
      });
    }else{
      return showMessage({
        message: t('common:buy_error'),
        type: 'danger',
      });
    }
  };

  onChange = (key, value) => {
    this.setState({
      [key]: value,
    });
  };

  images = () => {
    const {product, variation, images} = this.state;
    if (
      product.get('type') === productType.VARIABLE &&
      variation &&
      variation.get('image')
    ) {
      let list = [];
      const image = variation.get('image');
      if (image) {
        list.push(image.toJS());
      }
      return fromJS(list);
    }
    return images;
  };

  showPrice = () => {
    const {currency, defaultCurrency} = this.props;
    const {product, variation} = this.state;

    let price_format = product.price;
    let p = product;
    return (
      <View style={styles.viewPrice}>
        <Price
          price_format={this.state.current_price}
          // type={type}
          h4
          isPercentSale
          // style={styles.viewPrice}
        />
        <ProductStock
          product={p}
          // style={p.get('type') !== productType.SIMPLE && styles.viewStock}
        />
      </View>
    );
  };

  showInfoType = () => {
    const {attribute} = this.props;
    const {product, meta_data, variations, loadingVariation} = this.state;
    if (product.get('type') === productType.EXTERNAL) {
      return <ProductExternal product={product} />;
    }
    if (product.get('type') === productType.GROUPED) {
      return <ProductGrouped product={product} />;
    }
    if (product.get('type') === productType.VARIABLE) {
      return (
        <ProductVariable
          loading={attribute.get('loading') || loadingVariation}
          meta_data={meta_data}
          productVariations={variations}
          productAttributes={product.get('attributes')}
          onChange={this.onChange}
          attribute={attribute.get('data')}
        />
      );
    }
    return null;
  };

  onAddCount = (count, price, id, data) => {
    let obj = this.state.priceList;
    obj[id.toString()] = {
      count,
      price,
      size: data.size,
      fix_price: data.fix_price,
    };
    this.setState({
      priceList: obj,
      quantity: count,
    });
  };

  onMinusCount = (count, price, id, data) => {
    let obj = this.state.priceList;
    obj[id.toString()] = {
      count,
      price,
      size: data.size,
      fix_price: data.fix_price,
    };
    this.setState({
      priceList: obj,
      quantity: count,
    });
  };
  render() {
    const {
      screenProps: {t, theme},
      navigation,
      configs,
      lang,
    } = this.props;

    const {product, images} = this.state;
    logger.log('product123', product?.buy_min, typeof product?.buy_min)
    const step = parseInt(product?.buy_step, 10);

    if (!product.id) {
      return (
        <ThemedView isFullView>
          <Empty
            title={t('empty:text_title_product_detail')}
            subTitle={t('empty:text_subtitle_product_detail')}
            clickButton={() => navigation.navigate(homeTabs.shop)}
          />
        </ThemedView>
      );
    }
    const isExist = product.quantity > 0;
    return (
      <ScrollProductDetail
        headerTitle={product.name}
        imageElement={
          <ProductImages
            images={images}
            product_id={product.id}
            url={product.url}
            name_product={product.name}
            height={HEADER_MAX_HEIGHT}
          />
        }
        heightViewImage={HEADER_MAX_HEIGHT}>
        <Container style={styles.container}>
          <Text h2 medium style={styles.textName}>
            {lang !== 'ua'
              ? product.i18ns[0]?.name || product.name
              : product.i18ns[1]?.name || product.name}
          </Text>
          {this.showPrice()}
          {configs.get('toggleShortDescriptionProduct') && product.content ? (
            <View style={{marginBottom: 16}}>
              <TextHtml
                value={
                  lang !== 'ua'
                    ? product.i18ns[0]?.content || product.content
                    : product.i18ns[1]?.content || product.content
                }
                style={merge(
                  changeSize('h6'),
                  changeColor(theme.Text.third.color),
                )}
              />
            </View>
          ) : null}
          <View>
            {product.characteristics?.length > 0 &&
              product.characteristic_prices?.length > 0 &&
              product.characteristics[0].values.map((value, index) => {
                return (
                  <View
                    key={`${value.id}_${index}`}
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                      marginTop: index === 0 ? 0 : 30,
                      width: Dimensions.get('window').width,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignSelf: 'center',
                        width: '70%',
                      }}>
                      {lang === 'ua' && (
                        <View
                          style={{
                            flexDirection: 'column',
                            justifyContent: 'center',
                          }}>
                          <Text h5>
                            {t('empty:text_size')}: {value.i18ns[1].value};{' '}
                          </Text>
                          <Text h5>
                            {t('empty:text_price')}:{' '}
                            <Text style={{fontWeight: 'bold', fontSize: 17}}>
                              {product.characteristic_prices[index].price} ₴
                            </Text>
                          </Text>
                        </View>
                      )}
                      {lang !== 'ua' && (
                        <View
                          style={{
                            flexDirection: 'column',
                            justifyContent: 'center',
                          }}>
                          <Text h5>
                            {t('empty:text_size')}: {value.i18ns[0].value};{' '}
                          </Text>
                          <Text h5>
                            {t('empty:text_price')}:{' '}
                            {product.characteristic_prices[index].price} ₴
                          </Text>
                        </View>
                      )}
                    </View>
                    <View style={{width: '30%'}}>
                      <CounterPrice
                        text={product.buy_step}
                        counterDefault={product?.buy_min}
                        step={step}
                        price={parseInt(
                          product.characteristic_prices[index].price,
                          10,
                        )}
                        id={product.characteristic_prices[index].id}
                        onAddCount={(count, price, id) =>
                          this.onAddCount(count, price, id, {
                            size: value.i18ns[0].value,
                            fix_price:
                              product.characteristic_prices[index].price,
                          })
                        }
                        onMinusCount={(count, price, id) =>
                          this.onMinusCount(count, price, id, {
                            size: value.i18ns[0].value,
                            fix_price:
                              product.characteristic_prices[index].price,
                          })
                        }
                      />
                    </View>
                  </View>
                );
              })}
            {product.characteristics?.length === 0 && (
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: Dimensions.get('window').width,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    width: '70%',
                  }}>
                  <Text
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      flexWrap: 'wrap',
                    }}>
                    <Text>Шт.:
                    </Text>
                  </Text>
                </View>
                <View style={{width: '30%'}}>
                  <CounterPrice
                    text={product.buy_step}
                    counterDefault={product?.buy_min}
                    step={step}
                    price={parseInt(product.price, 10)}
                    id={product.id}
                    onAddCount={(count, price, id) =>
                      this.onAddCount(count, price, id, {
                        size: '',
                        fix_price: product.price,
                      })
                    }
                    onMinusCount={(count, price, id) =>
                      this.onMinusCount(count, price, id, {
                        size: '',
                        fix_price: product.price,
                      })
                    }
                  />
                </View>
              </View>
            )}
          </View>
        </Container>
        {!isExist && (
          <Text style={{textAlign: 'center'}}>{t('product:not_has')}</Text>
        )}
        {isExist && (
          <Container style={styles.viewFooter}>
            <Button
              title={t('common:text_add_cart')}
              onPress={this.addToCart}
            />
          </Container>
        )}
        {/*)}*/}
      </ScrollProductDetail>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: margin.large,
  },
  viewCategoryRating: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginBottom: margin.small / 2,
  },
  textCategory: {
    flex: 1,
    marginRight: margin.base,
  },
  viewRating: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textRating: {
    fontSize: 10,
    lineHeight: 15,
    marginLeft: margin.small / 2,
  },
  textName: {
    marginBottom: margin.small,
  },
  viewPrice: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: margin.large,
  },
  textDescription: {
    marginBottom: margin.large,
  },
  viewStock: {
    marginTop: margin.large,
    marginBottom: margin.small + margin.big,
  },
  viewRelated: {
    marginBottom: margin.big,
  },
  viewFooter: {
    marginVertical: margin.big,
  },
});

const mapStateToProps = state => {
  return {
    attribute: attributeSelector(state),
    dataRating: dataRatingSelector(state),
    currency: currencySelector(state),
    defaultCurrency: defaultCurrencySelector(state),
    lang: languageSelector(state),
    configs: configsSelector(state),
    vendorDetail: detailVendorSelector(state),
    order_data: selectOrderData(state),
  };
};

const withReduce = connect(mapStateToProps);

export default compose(
  withReduce,
  defaultPropsData,
  getSingleData,
  withLoading,
)(Product);
