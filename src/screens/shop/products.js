import React from 'react';
import {connect} from 'react-redux';
import concat from 'lodash/concat';
import find from 'lodash/find';
import flatMap from 'lodash/flatMap';
import compact from 'lodash/compact';
import unescape from 'lodash/unescape';

import {Map, fromJS} from 'immutable';

import {View, StyleSheet, ActivityIndicator} from 'react-native';

import {Header, ThemedView} from 'src/components';
import Container from 'src/containers/Container';
import {TextHeader, IconHeader, CartIcon} from 'src/containers/HeaderComponent';
import Loading from 'src/containers/Loading/LoadingDefault';
import Empty from 'src/containers/Empty';

import Refine from './containers/Refine';
import SwitchProduct from './containers/SwitchProduct';
import ProductView from './containers/ProductView';
import CategoryList from './product/CategoryList';

import {sortBySelector, filterBySelector} from 'src/modules/product/selectors';
import {languageSelector} from 'src/modules/common/selectors';

import {
  clearFilter,
  fetchProducts as clearData,
} from 'src/modules/product/actions';
import {getProductsCategories} from 'src/modules/product/service';

import {margin} from 'src/components/config/spacing';
import {mainStack, homeTabs} from 'src/config/navigator';
import {categorySelector} from 'src/modules/category/selectors';
import logger from '../../utils/logger';

const findCategory = (categoryId = '', lists = []) => {
  if (!categoryId || !lists || lists.length < 1) {
    return null;
  }
  var loopWhile = true;

  var category = null;
  var listFlat = lists;

  while (loopWhile && listFlat.length > 0) {
    const categoryFind = find(listFlat, c => c.id == categoryId);
    if (categoryFind) {
      category = categoryFind;
      loopWhile = false;
    } else {
      listFlat = compact(flatMap(listFlat, ca => ca.categories));
    }
  }
  return category;
};

class ProductsScreen extends React.Component {
  static navigationOptions = {
    headerShown: false,
  };

  constructor(props, context) {
    super(props, context);
    const {
      navigation,
      screenProps: {t},
      categories,
    } = props;

    const categoryId = navigation.getParam('id', '');
    const category = findCategory(categoryId, categories);
    const name = navigation.getParam('name', '')
      ? navigation.getParam('name', '')
      : category && category.name
      ? category.name
      : t('common:text_product');

    this.state = {
      category,
      name,
      loading: true,
      refreshing: false,
      loadingMore: false,
      data: [],
      page: 1,
      page_limit: 0,
      activeSortItem: '',
      sortData: [
        {
          key: 'popularity',
          name: t('catalog:text_sort_popular'),
          query: {orderby: 'is_top_sale'},
        },
        {
          key: 'price',
          name: t('catalog:text_sort_price_low'),
          query: {order: 'asc', orderby: 'price'},
        },
        {
          key: 'price-desc',
          name: t('catalog:text_sort_price_high'),
          query: {order: 'desc', orderby: '-price'},
        },
      ],
    };
  }

  componentDidMount() {
    this.fetchProducts();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState !== this.state) {
      return;
    }
    const {navigation} = this.props;

    const sort = navigation.getParam('sortBy', Map());
    const filter = navigation.getParam('filterBy', Map());

    const prevSort = prevProps.navigation.getParam('sortBy', Map());
    const prevFilter = prevProps.navigation.getParam('filterBy', Map());

    if (!sort.equals(prevSort) || !filter.equals(prevFilter)) {
      this.fetchProducts(1);
    }
  }

  componentWillUnmount() {
    this.props.dispatch(clearData([]));
    this.props.dispatch(clearFilter());
  }

  getData = page => {
    const {sortBy, filterBy, lang, navigation} = this.props;
    const {category} = this.state;

    let query;

    if (category) {
      query = Map({
        lang: lang,
        per_page: 20,
        status: 1,
        page: page,
        sort: '-created_at',
      })
        // .merge(sortBy.get('query'))
        .merge(filterBy)
        .merge(navigation.getParam('filterBy', Map()))
        .set(
          'filter[category_id]',
          filterBy.get('category')
            ? filterBy.get('category')
            : category && category.id
            ? category.id
            : '',
        )
        .set(
          'sort',
          sortBy.toJS().query
            ? sortBy.toJS().query.orderby + ',-created_at'
            : '-created_at',
        )
        .set(
          'expand',
          'categories,images,thumbnails,characteristics,characteristic_prices,out_of_stock_status,url',
        )
        .set('filter[price][gte]', filterBy.toJS().min_price)
        .set('filter[price][lte]', filterBy.toJS().max_price)
        .set('filter[status]', 1);
    } else {
      query = Map({
        lang: lang,
        per_page: 20,
        status: 1,
        page: page,
        sort: '-created_at',
      })
        // .merge(sortBy.get('query'))
        .merge(filterBy)
        .merge(navigation.getParam('filterBy', Map()))
        .set(
          'filter[category_id]',
          filterBy.get('category')
            ? filterBy.get('category')
            : category && category.id
            ? category.id
            : '',
        )
        .set(
          'sort',
          sortBy.toJS().query
            ? sortBy.toJS().query.orderby + ',-created_at'
            : '-created_at',
        )
        .set(
          'expand',
          'categories,images,thumbnails,characteristics,characteristic_prices,out_of_stock_status,url',
        )
        .set('filter[price][gte]', filterBy.toJS().min_price)
        .set('filter[price][lte]', filterBy.toJS().max_price)
        .set('filter[is_action]', 1);
    }

    return getProductsCategories(query.toJS());
  };

  fetchProducts = async (page = this.state.page) => {
    const {sortBy} = this.props;
    try {
      this.setState({loading: true});
      const fetchGet = await this.getData(page);
      this.setState({page_limit: fetchGet.res_fetch._meta.pageCount});
      const dataGet = fetchGet.res;

      this.setState({activeSortItem: sortBy.toJS().key});

      if (dataGet.length > 1) {
        this.setState(preState => {
          return {
            refreshing: false,
            loadingMore: dataGet.length === 4,
            data:
              page === 1 ? dataGet : concat(preState.data, dataGet.reverse()),
          };
        });
      } else {
        this.setState(preState => {
          return {
            loadingMore: false,
            refreshing: false,
            data: page === 1 ? [] : preState.data,
          };
        });
      }
    } catch (e) {
      this.setState({
        loading: false,
      });
    } finally {
      this.setState({loading: false});
    }
  };

  handleCategoryPress = value => {
    this.props.navigation.push(mainStack.products, {
      id: value.id,
      name: unescape(value.name),
    });
  };

  handleLoadMore = () => {
    const {loadingMore, page_limit, page} = this.state;

    if (page_limit <= page) {
      return this.setState({loadingMore: false});
    }
    if (!loadingMore && page_limit >= page) {
      this.setState(
        prevState => ({
          page: prevState.page + 1,
          loadingMore: true,
          status: 1,
        }),
        () => {
          this.fetchProducts();
        },
      );
    }
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true,
      },
      () => {
        this.fetchProducts();
      },
    );
  };

  render() {
    const {
      navigation,
      screenProps: {t},
      categories,
    } = this.props;
    const {category, name, data, loading, loadingMore, refreshing} = this.state;

    return (
      <ThemedView isFullView>
        <Header
          leftComponent={<IconHeader />}
          centerComponent={<TextHeader title={name} />}
          rightComponent={<CartIcon />}
        />
        {/* {loading && (
          <View>
            <ActivityIndicator size="large" />
          </View>
        )} */}
        {data.length > 0 && (
          <View style={styles.viewList}>
            <Container style={styles.viewRefineSwitch}>
              <Refine
                onPress={() =>
                  navigation.navigate(mainStack.refine, {
                    category: category,
                    data,
                  })
                }
              />
              <SwitchProduct />
            </Container>
            <CategoryList
              onPress={this.handleCategoryPress}
              data={categories && categories.length > 0 ? categories : null}
            />
            <ProductView
              data={fromJS(data)}
              loadingMore={loadingMore}
              refreshing={refreshing}
              handleLoadMore={this.handleLoadMore}
              handleRefresh={this.handleRefresh}
            />
          </View>
        )}
        {data.length <= 0 && !loading && (
          <Empty
            icon="box"
            title={t('empty:text_title_product')}
            subTitle={t('empty:text_subtitle_product')}
            titleButton={t('common:text_go_shopping')}
            clickButton={() => navigation.navigate(homeTabs.shop)}
          />
        )}
      </ThemedView>
    );
  }
}

const styles = StyleSheet.create({
  viewRefineSwitch: {
    marginTop: margin.base,
    marginBottom: margin.large,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewList: {
    flex: 1,
  },
});

const mapStateToProps = state => {
  const {data} = categorySelector(state);
  return {
    sortBy: sortBySelector(state),
    filterBy: filterBySelector(state),
    lang: languageSelector(state),
    categories: data,
  };
};

export default connect(mapStateToProps)(ProductsScreen);
