import React, {useState} from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {Text, ThemedView} from 'src/components';
import {margin, borderRadius} from 'src/components/config/spacing';
import logger from '../../../utils/logger';

const CounterPrice = ({
  counterDefault,
  text,
  onAddCount,
  onMinusCount,
  price,
  id,
  step,
}) => {
  const [counter, setCounter] = useState(0);
  logger.log('counterDefault', counterDefault)

  const onAdd = () => {
    const add =
      counter === 0 ? parseInt(counterDefault, 10) : counter + step || 1;
    setCounter(add);
    onAddCount && onAddCount(add, price * add, id);
  };
  const onMinus = () => {
    if (counterDefault >= counter) {
      logger.log('on minus')
      setCounter(0);
      onMinusCount && onMinusCount(minus, price * minus, id);
      return;
    }
    let minus = counter - parseInt(text, 10);
    // if (minus <= parseInt(counterDefault, 10)) {
    //   minus = 0;
    // }
    setCounter(minus);
    onMinusCount && onMinusCount(minus, price * minus, id);
  };
  return (
    <View style={styles.container}>
      <ThemedView style={[styles.container]} colorSecondary>
        <Text style={styles.text}>{counter}</Text>
        <TouchableOpacity style={[styles.touch, styles.left]} onPress={onMinus}>
          <Text h4 medium>
            -
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.touch, styles.right]} onPress={onAdd}>
          <Text h4 medium>
            +
          </Text>
        </TouchableOpacity>
      </ThemedView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    minWidth: 86,
    maxWidth: 90,
    borderRadius: borderRadius.base,
    overflow: 'hidden',
  },
  touch: {
    width: margin.big,
    height: '100%',
    position: 'absolute',
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  left: {
    left: 0,
    paddingLeft: 4,
  },
  right: {
    right: 0,
    paddingRight: 4,
  },
  text: {
    textAlign: 'center',
    marginHorizontal: margin.big,
    paddingVertical: 4,
  },
});

export default CounterPrice;
