import React from 'react';
import {withNavigation} from 'react-navigation';
import {connect} from 'react-redux';

import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {withTheme, Text, Image} from 'src/components';
import InfoViewer from './InfoViewer';
import moment from 'moment';
import ru from 'moment/locale/ru'
import uk from 'moment/locale/uk'
import {blogStack} from 'src/config/navigator';
import {margin, padding, borderRadius} from 'src/components/config/spacing';
import {languageSelector} from 'src/modules/common/selectors';

import {timeAgo} from 'src/utils/time';

const ItemBlog = ({navigation, item, theme, width, style, tz, language}) => {
  console.log('language', language)
  if (!item || typeof item !== 'object') {
    return null;
  }
  const imageStyle = {
    width,
    flex: 1,
  };
  return (
    <TouchableOpacity
      style={[
        styles.container,
        {
          borderColor: theme.colors.border,
        },
        style && style,
      ]}
      onPress={() =>
        navigation.navigate(blogStack.blog_detail, {blog: item, type: 'blog'})
      }>
      <Image
        source={
          item.thumbnail
            ? {uri: item.thumbnail}
            : require('src/assets/images/pDefault.png')
        }
        // resizeMode="contain"
        style={imageStyle}
        containerStyle={styles.viewImage}
      />
      <View style={styles.viewRight}>
        <Text h4 medium style={styles.name}>
          {item.name && item.name}
        </Text>
        {item && item.published_at && (
          <Text h6 colorThird style={styles.time}>
            {/* {timeAgo(moment(item.published_at).locale('ua'), tz)} */}
            {moment(item.published_at)
              .locale(language === 'ua' ? 'uk' : 'ru')
              .fromNow()}
          </Text>
        )}
        <InfoViewer categories={item._categories} urlUser={item.author_url} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingBottom: padding.big,
    borderBottomWidth: 1,
  },
  viewImage: {
    borderRadius: borderRadius.base,
    overflow: 'hidden',
  },
  viewRight: {
    marginLeft: margin.large,
    flex: 1,
  },
  name: {
    marginBottom: margin.small,
  },
  time: {
    marginBottom: margin.big - 4,
  },
});

ItemBlog.defaultProps = {
  width: 137,
  height: 123,
};

const mapStateToProps = state => {
  return {
    language: languageSelector(state),
  };
};

export default withNavigation(withTheme(connect(mapStateToProps)(ItemBlog)));
