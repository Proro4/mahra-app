import React from 'react';

import {connect} from 'react-redux';

import {DrawerActions} from 'react-navigation-drawer';
import {StyleSheet, View, FlatList, ActivityIndicator} from 'react-native';
import {Header, ThemedView} from 'src/components';
import {IconHeader, Logo, CartIcon} from 'src/containers/HeaderComponent';
import LastestBlog from './containers/LastestBlog';
import ItemBlog from './containers/ItemBlog';

import {getBlogs} from 'src/modules/blog/service';
import {getSiteConfig, languageSelector} from 'src/modules/common/selectors';

import {padding, margin} from 'src/components/config/spacing';
import {prepareBlogItem} from 'src/utils/blog';
import logger from '../../utils/logger';

class BlogList extends React.Component {
  state = {
    data: [],
    page: 1,
    totalCount: 11,
    loading: true,
    loadingMore: false,
    refreshing: false,
    error: null,
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async refresh => {
    try {
      const {language} = this.props;
      const {page} = this.state;
      const data = await getBlogs(`page=${page}`);
      if (this.state.data.length >= this.state.totalCount) {
        return this.setState({
          loadingMore: false,
          loading: false,
        });
      }

      this.setState({
        totalCount: data._meta.totalCount,
      });
      if (data.items.length > 0) {
        const prepareData = [];

        data.items.map(value => {
          prepareData.push({...value, ...value.i18ns[0]});
        });
        // const list = data.map(v => prepareBlogItem(v));
        this.setState(prevState => ({
          data: refresh
            ? [...prepareData]
            : [...prevState.data, ...prepareData],
          loading: false,
          loadingMore: data.length === 10,
          refreshing: false,
        }));
      } else {
        this.setState({
          loadingMore: false,
          loading: false,
        });
      }
    } catch (error) {
      this.setState({
        error,
        loading: false,
        loadingMore: false,
      });
    }
  };

  handleLoadMore = () => {
    const {loadingMore} = this.state;

    if (!loadingMore) {
      this.setState(
        (prevState, nextProps) => ({
          page: prevState.page + 1,
          loadingMore: true,
        }),
        () => {
          this.fetchData();
        },
      );
    }
  };

  renderFooter = () => {
    if (!this.state.loadingMore) {
      return null;
    }
    console.log('!this.state.loadingMore', !this.state.loadingMore)

    return (
      <View style={styles.footerFlatlist}>
        <ActivityIndicator animating size="small" />
      </View>
    );
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true,
      },
      () => {
        this.fetchData(true);
      },
    );
  };

  render() {
    const {siteConfig} = this.props;
    const {data} = this.state;
    const dataSwiper = data.filter((item, index) => index < 3);
    const dataFlatlist = data.filter((item, index) => index > 0);
    logger.log('fetchData', data);

    return (
      <ThemedView isFullView>
        <Header
          leftComponent={
            <IconHeader
              name="align-left"
              size={22}
              onPress={() =>
                this.props.navigation.dispatch(DrawerActions.openDrawer())
              }
            />
          }
          centerComponent={<Logo />}
          rightComponent={<CartIcon />}
        />
        {!this.state.loading ? (
          <FlatList
            data={data}
            keyExtractor={item => `${item.id}`}
            renderItem={({item, index}) => {
              return (
                <ItemBlog
                  tz={siteConfig.get('timezone_string')}
                  item={item}
                  style={[
                    styles.item,
                    {borderTopWidth: index === 0 ? 1 : 0},
                    index === data.length - 1 && {borderBottomWidth: 0},
                  ]}
                />
              );
            }}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.5}
            initialNumToRender={10}
            // ListHeaderComponent={<LastestBlog data={dataSwiper} />}
            ListFooterComponent={this.renderFooter()}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
          />
        ) : (
          <View style={styles.viewLoading}>
            <ActivityIndicator />
          </View>
        )}
      </ThemedView>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    paddingTop: padding.big,
    marginHorizontal: margin.large,
  },
  viewLoading: {
    marginVertical: margin.large,
  },
  footerFlatlist: {
    position: 'relative',
    height: 40,
    justifyContent: 'center',
  },
});

const mapStateToProps = state => {
  return {
    language: languageSelector(state),
    siteConfig: getSiteConfig(state),
  };
};

export default connect(mapStateToProps)(BlogList);
