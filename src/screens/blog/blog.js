import React from 'react';

import {compose} from 'recompose';
import {connect} from 'react-redux';
import merge from 'lodash/merge';

import {
  StyleSheet,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  Share,
} from 'react-native';
import {Header, Icon, Text, ThemedView} from 'src/components';
import Container from 'src/containers/Container';
import TextHtml from 'src/containers/TextHtml';
import Empty from 'src/containers/Empty';
import InfoViewer from './containers/InfoViewer';
import {IconHeader, TextHeader} from 'src/containers/HeaderComponent';

import {defaultPropsData, getSingleData} from 'src/hoc/single-data';
import {withLoading} from 'src/hoc/loading';

import {timeAgo} from 'src/utils/time';
import {padding, margin, borderRadius} from 'src/components/config/spacing';
import {changeLineHeight, changeColor} from 'src/utils/text-html';

import {blogStack} from 'src/config/navigator';
import {languageSelector} from 'src/modules/common/selectors';
import logger from '../../utils/logger';

const {width} = Dimensions.get('window');
const WIDTH_IMAGE = width - 2 * padding.large;
const MIN_HEIGHT_IMAGE = (WIDTH_IMAGE * 304) / 344;

const marginSmall = margin.small;
const marginLarge = margin.large;
const marginBig = margin.base + margin.large;

class BlogDetail extends React.Component {
  share = () => {
    const {navigation} = this.props;
    const blog = navigation.getParam('blog', {});
    let message =
      Platform.OS === 'android'
        ? `${blog.name}. ${blog._links.self.href}`
        : blog.name;

    Share.share(
      {
        message: message,
        url: blog._links.self.href,
        title: 'Share',
      },
      {
        // // Android only:
        dialogTitle: 'Махровый мир',
        // // iOS only:
        // excludedActivityTypes: [
        //   'com.apple.UIKit.activity.PostToTwitter'
        // ]
      },
    );
  };

  renderData = () => {
    const {
      screenProps: {t, theme},
      navigation,
    } = this.props;
    const blog = navigation.getParam('blog', {});
    // const {blog} = this.state;

    if (!blog || !blog.id) {
      return (
        <Empty
          title={'Get data fail'}
          subTitle={'Check id blog'}
          titleButton="Go Blogs"
          clickButton={() => navigation.navigate(blogStack.blog_list)}
        />
      );
    }
    logger.log('blog', blog);
    return (
      <ScrollView>
        <Container>
          <Image
            source={
              blog.thumbnail
                ? {uri: blog.thumbnail}
                : require('src/assets/images/pDefault.png')
            }
            resizeMode="contain"
            style={styles.image}
          />
          {blog && blog.published_at && (
            <Text h6 colorThird style={styles.textTime}>
              {timeAgo(new Date(blog.published_at))}
            </Text>
          )}
          {blog.name && (
            <Text h2 medium style={styles.textTitle}>
              {blog.name}
            </Text>
          )}
          {/*<TextHtml value={text} style={styleHtml(theme)} />*/}
          <InfoViewer categories={blog._categories} user={blog.author_url} />
          <View style={styles.viewDesciption}>
            <TextHtml
              value={blog.content}
              style={merge(
                changeColor(theme.Text.secondary.color),
                changeLineHeight(28),
              )}
            />
          </View>
          <View style={styles.viewShare}>
            <TouchableOpacity style={styles.buttonShare} onPress={this.share}>
              <Icon name="share" size={20} />
              <Text colorSecondary style={styles.textShare}>
                {t('blog:text_share')}
              </Text>
            </TouchableOpacity>
          </View>
        </Container>
      </ScrollView>
    );
  };
  render() {
    const {
      screenProps: {t},
    } = this.props;

    return (
      <ThemedView isFullView>
        <Header
          leftComponent={<IconHeader />}
          centerComponent={<TextHeader title={t('common:text_blog')} />}
        />
        {this.renderData()}
      </ThemedView>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: WIDTH_IMAGE,
    minHeight: MIN_HEIGHT_IMAGE,
    borderRadius: borderRadius.large,
    marginBottom: marginLarge,
  },
  textTime: {
    marginBottom: marginSmall,
  },
  textTitle: {
    marginBottom: marginSmall,
  },
  viewDesciption: {
    marginVertical: margin.big + margin.base,
  },
  viewShare: {
    alignItems: 'flex-end',
    marginBottom: marginBig,
  },
  buttonShare: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textShare: {
    marginLeft: margin.small,
  },
});

const mapStateToProps = state => {
  return {
    lang: languageSelector(state),
  };
};

const withReduce = connect(mapStateToProps);

export default compose(
  withReduce,
  defaultPropsData,
  getSingleData,
  withLoading,
)(BlogDetail);
