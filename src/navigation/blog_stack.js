import React from 'react';

import {blogStack} from 'src/config/navigator';

import {createBottomTabNavigator} from 'react-navigation-tabs';

import BlogList from 'src/screens/blog/blogs';
import BlogDetail from 'src/screens/blog/blog';
import Tabbar from 'src/containers/Tabbar';

export default createBottomTabNavigator(
  {
    [blogStack.blog_list]: BlogList,
    [blogStack.blog_detail]: BlogDetail,
    // {
    //   defaultNavigationOptions: {
    //     headerShown: false,
    //   },
    // },
  },
  {
    defaultNavigationOptions: {
      // tabBarVisible: false
    },
    tabBarComponent: props => <Tabbar {...props} />,
  },
);
