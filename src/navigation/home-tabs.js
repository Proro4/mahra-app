import React from 'react';

import {createBottomTabNavigator} from 'react-navigation-tabs';

import Home from 'src/screens/home';
import Category from 'src/screens/shop/category';
import Product from 'src/screens/shop/product';
import WishList from 'src/screens/wishlist';

import ProfileStack from './profile-stack';
import CartStack from './cart-stack';

import Tabbar from 'src/containers/Tabbar';

import {homeTabs} from 'src/config/navigator';
    console.log('Product- ', Product)
    console.log('Category- ', Category)

const Tabs = createBottomTabNavigator(
  {
    [homeTabs.home]: Home,
    [homeTabs.shop]: Category,
    [homeTabs.wish_list]: WishList,
    [homeTabs.cart]: {
      screen: CartStack,
      navigationOptions: ({navigation}) => {
        const {
          state: {index},
        } = navigation;
        return {
          tabBarVisible: index === 0,
        };
      },
    },
    [homeTabs.me]: ProfileStack,
  },
  {
    defaultNavigationOptions: {
      // tabBarVisible: true,
    },
    tabBarComponent: props => <Tabbar {...props} />,
  },
);

export default Tabs;
