import en from './en';
import ar from './ar';
import ru from './ru';
import ua from './ua';

export default {
  en,
  ar,
  ru,
  ua,
};
