import request, {request_custom} from 'src/utils/request';
import queryString from 'query-string';
import logger from '../../utils/logger';

/**
 * Fetch blog data
 * @returns {*}
 */

export const getBlogs = query => {
  query = {...query, 'filter[status]': 1};

  return request_custom.get(
    `articles?${queryString.stringify(query, {arrayFormat: 'comma'})}`,
  );
  // return request.get(
  //   `/wp/v2/posts?${queryString.stringify(query, {arrayFormat: 'comma'})}`,
  // );
};
