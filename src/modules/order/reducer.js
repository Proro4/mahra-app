import {fromJS} from 'immutable';
import * as Actions from './constants';
import {CLEAR_CART} from 'src/modules/cart/constants';
import logger from '../../utils/logger';

export const initState = fromJS({
  order: {},
  pending: false,
  data: [],
  loading: false,
  updateOrderPending: false,
  loadingRefund: false,
});

function orderReducer(state = initState, {type, payload}) {
  switch (type) {
    // Create Order

    case Actions.SET_ORDER_DATA:
      return state.set('data', fromJS(payload));
    case Actions.CHANGE_ITEM:
      logger.log('CHANGE_ITEM');
      let arr2 = state.toJS().data;

      state.toJS().data.filter((value, index) => {
        if (value.id === payload.item.id && value.size === payload.item.size) {
          const price = parseFloat(value.fix_price);
          return (arr2[index] = {
            ...payload.item,
            quantity: payload.quantity,
            price: price * payload.quantity,
          });
        } else {
          return value;
        }
      });
      return state.set('data', fromJS([...arr2]));
    case Actions.UPDATE_ORDER_DATA:
      const arr = state.toJS().data;
      let isChange = true;
      state.toJS().data.forEach((value, index) => {
        if (
          value &&
          value.product_id &&
          value.product_id === payload.product_id &&
          value.size === payload.size
        ) {
          isChange = false;
          logger.log(`payload ${index}`, payload.size === value.size);
          return (arr[index] = payload);
        }
      });

      if (isChange) {
        return state.set('data', fromJS([...arr, payload]));
      }

      return state.set('data', fromJS([...arr]));
    case Actions.REMOVE_ORDER_DATA:
      const res = state.toJS().data;
      state.toJS().data.forEach((value, index) => {
        if (value.id === payload.id && value.size === payload.size) {
          res.splice(index, 1);
          return;
        }
        // res.push(value);
        // return value.id !== payload.id || value.size !== payload.size;
      });
      logger.log('REMOVE_ORDER_DATA', res);

      return state.set('data', fromJS([...res]));
    case Actions.CREATE_ORDER:
      console.log('CREATE_ORDER', payload);
      return state.set('pending', true);
    case Actions.CREATE_ORDER_SUCCESS:
      return state.set('order', [payload]);
    case Actions.CREATE_ORDER_SUCCESS:
      console.log('UPDATE_ORDER_SUCCESS', state.toJS());
      return state.set('order', [...state.toJS(), payload]);
    case Actions.CREATE_ORDER_ERROR:
      return state.set('pending', false);

    // Update Order
    case Actions.UPDATE_ORDER:
      return state
        .set('updateOrderPending', true)
        .set('order', initState.get('order'));
    case Actions.UPDATE_ORDER_SUCCESS:
    case Actions.UPDATE_ORDER_ERROR:
      return state.set('updateOrderPending', false);

    // Refund Order
    case Actions.REFUND_ORDER:
      return state.set('loadingRefund', true);
    case Actions.REFUND_ORDER_SUCCESS:
    case Actions.REFUND_ORDER_ERROR:
      return state.set('loadingRefund', false);

    case CLEAR_CART:
      return state.set('order', initState.get('order'));

    default:
      return state;
  }
}

export default orderReducer;
