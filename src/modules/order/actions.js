// @flow
import * as Actions from './constants';
import logger from '../../utils/logger';

type Action = {type: string, payload?: any, error?: any};

/**
 * Action create an order
 * @returns {{type: string}}
 */
export function createOrder() {
  return {
    type: Actions.CREATE_ORDER,
  };
}

export function changeQuantity(item, quantity) {
  return {
    type: Actions.CHANGE_ITEM,
    payload: {
      item,
      quantity,
    },
  };
}

export function setOrderData(payload) {
  return {
    type: Actions.SET_ORDER_DATA,
    payload,
  };
}

export function updateOrderData(payload) {
  return {
    type: Actions.UPDATE_ORDER_DATA,
    payload,
  };
}

export function removeOrderData(payload) {
  return {
    type: Actions.REMOVE_ORDER_DATA,
    payload,
  };
}

export function createOrderSuccess(payload) {
  return {
    type: Actions.CREATE_ORDER_SUCCESS,
    payload,
  };
}

/**
 * Update an order
 * @param id: order id
 * @returns {{type: string, payload: {id: *}}}
 */
export function updateOrder(id: number): Action {
  return {
    type: Actions.UPDATE_ORDER,
    payload: {
      id,
    },
  };
}

/**
 * Refund an order
 * @param id: order id
 * @param amount: price
 * @returns {{type: string, payload: {id: *, amount: *}}}
 */
export function refundOrder(id: number, amount: string): Action {
  return {
    type: Actions.REFUND_ORDER,
    payload: {
      id,
      amount,
    },
  };
}
