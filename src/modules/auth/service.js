import queryString, {stringify} from 'query-string';
import request, {request_custom} from 'src/utils/request';
import AsyncStorage from '@react-native-community/async-storage';
import logger from '../../utils/logger';

export const loginWithEmail = data => {
  return request_custom.post('/sign-in/index', data);
};

export const registerWithEmail = data =>
  request_custom.post('sign-in/signup', data);

export const getProfileData = async () => {
  const token = await AsyncStorage.getItem('@token');
  logger.log('token', token);
  return request_custom.get('profile/index', {
    headers: {Authorization: `Bearer ${token}`},
  });
};

export const updateCustomer = async (user_id, data) => {
  const token = await AsyncStorage.getItem('@token');
  const res = request_custom.put(
    'profile/index',
    {...data},
    {
      headers: {Authorization: `Bearer ${token}`},
    },
  );
  return res;
};

export const forgotPassword = async user_login => {
  const token = await AsyncStorage.getItem('@token');

  return request_custom.put(
    'sign-in/request-password-reset',
    {
      scenario: 'phone',
      phone: user_login,
    },
    {
      headers: {Authorization: `Bearer ${token}`},
    },
  );
};

export const loginWithMobile = idToken =>
  request.post('/rnlab-app-control/v1/login-otp', {idToken});

export const loginWithFacebook = token =>
  request.post('/rnlab-app-control/v1/facebook', {token});

export const loginWithGoogle = user =>
  request.post('/rnlab-app-control/v1/google', user);

export const loginWithApple = data =>
  request.post('/rnlab-app-control/v1/apple', data);

export const changePassword = ({password_old, password_new}) =>
  request.post('/rnlab-app-control/v1/change-password', {
    password_old,
    password_new,
  });
export const changeEmail = ({u_password, u_email}) =>
  request.patch('users/change-email', {u_password, u_email});

export const getCustomer = user_id =>
  request.get(`/wc/v3/customers/${user_id}`);

export const logout = () => request.get('users/logout');
export const isLogin = () => request.get('users/is-login');
export const checkPhoneNumber = data =>
  request.post('/rnlab-app-control/v1/check-phone-number', data);
export const checkInfo = data =>
  request.post('/rnlab-app-control/v1/check-info', data);

// Digits API

export const digitsCreateUser = data =>
  request.post('/digits/v1/create_user', queryString.stringify(data));

export const digitsLoginUser = data =>
  request.post('/digits/v1/login_user', queryString.stringify(data));

export const digitsRecoveryUser = data =>
  request.post('/digits/v1/recovery', queryString.stringify(data));

export const digitsLogoutUser = () => request.post('/digits/v1/logout');

export const digitsUpdatePhone = data =>
  request.post('/digits/v1/update_phone', queryString.stringify(data));

export const digitsSendOtp = data =>
  request.post('/digits/v1/send_otp', queryString.stringify(data));
export const sendOtp = data =>
  request.post('/digits/v1/send_otp', queryString.stringify(data));

export const digitsReSendOtp = data =>
  request.post('/digits/v1/resend_otp', queryString.stringify(data));

export const digitsVerifyOtp = data =>
  request.post('/digits/v1/verify_otp', queryString.stringify(data));
