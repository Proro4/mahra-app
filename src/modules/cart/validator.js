import {Map} from 'immutable';
import isMobilePhone from 'validator/lib/isMobilePhone';
import isEmail from 'validator/lib/isEmail';
import isLength from 'validator/lib/isLength';
import i18n from '../../config-i18n';
import logger from '../../utils/logger';

export function validatorAddress(shipping) {
  let errors = Map();
  if (
    !shipping.get('first_name') ||
    !isLength(shipping.get('first_name'), {min: 1, max: 32})
  ) {
    errors = errors.set('first_name', i18n.t('validators:text_first_name'));
  }

  if (
    !shipping.get('last_name') ||
    !isLength(shipping.get('last_name'), {min: 1, max: 32})
  ) {
    errors = errors.set('last_name', i18n.t('validators:text_last_name'));
  }

  // if (!shipping.get('city') || !isLength(shipping.get('city'), { min: 2, max: 128 })) {
  //   errors = errors.set('city', i18n.t('validators:text_message'));
  // }

  // if (
  //   !shipping.get('address_1') ||
  //   !isLength(shipping.get('address_1'), {min: 3, max: 128})
  // ) {
  //   errors = errors.set('address_1', i18n.t('validators:text_message'));
  // }

  // if (!shipping.get('postcode') && !isLength(shipping.get('postcode'), { min: 2, max: 10 })) {
  //   errors = errors.set('postcode', i18n.t('validators:text_message'));
  // }

  // if (!shipping.get('country')) {
  //   errors = errors.set('country', 'Please select country!');
  // }

  if (
    !shipping.get('phone') ||
    shipping.get('phone').replace(/ /g, '').length !== 13
  ) {
    errors = errors.set('phone', i18n.t('validators:text_phone'));
  }

  if (!shipping.get('email') || !isEmail(shipping.get('email'))) {
    errors = errors.set('email', i18n.t('validators:text_email'));
  }

  return errors;
}
