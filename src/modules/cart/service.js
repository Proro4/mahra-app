import request, {request_custom} from 'src/utils/request';
import queryString from 'query-string';

/**
 * Fetch all the shipping methods.
 * @returns {*}
 */

export const fetchCreateShippingMethod = params =>
  request_custom.post('order/index', params, {
    'Accept-Language': 'uk',
  });

export const fetchGetShippingMethod = () =>
  request_custom.get('shipments?filter[status]=1');

export const fetchShippingMethod = () => request.get('/wc/v3/shipping_methods');

/**
 * Fetch all the shipping methods from a shipping zone.
 * @param id
 * @returns {*}
 */
export const fetchShippingZoneMethod = id =>
  request.get(`/wc/v3/shipping/zones/${id}/methods`);

/**
 * Fetch all the locations of a shipping zone.
 * @param id
 * @returns {*}
 */
export const fetchShippingLocaltionMethod = id =>
  request.get(`/wc/v3/shipping/zones/${id}/locations`);

/**
 * Get Continent by Country code
 * @param cc: country code
 * @returns {*}
 */
export const getContinentCode = cc =>
  request.get(`/rnlab-app-control/v1/get-continent-code-for-country?cc=${cc}`);

/**
 * Get Zones
 * @returns {*}
 */
export const getZones = () => request.get('/rnlab-app-control/v1/zones');

/**
 * Get Zones
 * @returns {*}
 */
export const getCoupons = query =>
  request.get(
    `/wc/v3/coupons?${queryString.stringify(query, {arrayFormat: 'comma'})}`,
  );
