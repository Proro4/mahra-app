import request, {request_custom} from 'src/utils/request';
import queryString from 'query-string';
import pickBy from 'lodash/pickBy';
import logger from '../../utils/logger';
import configApi from 'src/config/api';

/**
 * Fetch product data
 * @returns {*}
 */

export const getProductsCategories = async (query, headers) => {
  const url = `products?${queryString.stringify(
    pickBy(query, item => item !== ''),
    {
      arrayFormat: 'comma',
    },
  )}`;
  const res_fetch = await request_custom.get(url, headers);
  const index_lang = query.lang === 'ua' ? 1 : 0;
  let res =
    res_fetch.items.length > 0
      ? res_fetch.items.map(value => {
          return {
            ...value,
            ...value.i18ns[index_lang],
          };
        })
      : [];

  if (res_fetch?.items[0]?.out_of_stock_status) {
    res =
      res_fetch.items.length > 0
        ? res_fetch.items.map(value => {
            const price = parseFloat(value.price);
            return {
              ...value,
              price: price.toFixed(2),
              ...value.out_of_stock_status[index_lang],
              ...value.i18ns[index_lang],
            };
          })
        : [];
  }
  return {res, res_fetch};
};

export const getProducts = async (query, headers) => {
  query = {
    ...query,
    'filter[status]': 1,
  };
  const url = `products?${queryString.stringify(
    pickBy(query, item => item !== ''),
    {
      arrayFormat: 'comma',
    },
  )}`;

  const res_fetch = await request_custom.get(url, headers);
  const index_lang = query.lang === 'ua' ? 1 : 0;
  let res =
    res_fetch.items.length > 0
      ? res_fetch.items.map(value => {
          return {
            ...value,
            ...value.i18ns[index_lang],
          };
        })
      : [];
  logger.log(`GET  ${configApi.API_CUSTOM}${url}`, res);
  if (res_fetch?.items[0]?.out_of_stock_status) {
    res =
      res_fetch.items.length > 0
        ? res_fetch.items.map(value => {
            const price = parseFloat(value.price);
            return {
              ...value,
              price: price.toFixed(2),
              ...value.out_of_stock_status[index_lang],
              ...value.i18ns[index_lang],
            };
          })
        : [];
  }
  return res;
};

/**
 * Fetch single product
 * @param id : product id
 * @returns {*}
 */
export const getSingleProduct = (id, lang) =>
  request.get(`/wc/v3/products/${id}?lang=${lang}`);

/**
 * Fetch single blog
 * @param id : blog id
 * @returns {*}
 */
export const getSingleBlog = (id, lang) =>
  request.get(`/wp/v2/posts/${id}?lang=${lang}`);

/**
 * Get top sellers
 * @param query
 * @returns {*}
 */
export const topSellers = query =>
  request.get(`/wc/v3/reports/top_sellers?${queryString.stringify(query)}`);

/**
 *  Get attributes
 * @returns {*}
 */
export const getAttributes = lang =>
  request.get(`/wc/v3/products/attributes?lang=${lang}`);

/**
 *  Get attribute terms
 * @returns {*}
 */
export const getAttributeTerms = attribute_id =>
  request.get(`/wc/v3/products/attributes/${attribute_id}/terms`);

/**
 *  Get product reviews
 * @returns {*}
 */
export const getProductReviews = product_id =>
  request.get(`/wc/v3/products/reviews?product=${product_id}`);

/**
 *  Get rating product reviews
 * @returns {*}
 */
export const getRatingProductReviews = product_id =>
  request.get(`/rnlab-app-control/v1/rating-count?product_id=${product_id}`);

/**
 *  Get rating product reviews
 * @returns {*}
 */
export const addProductReviews = data =>
  request.post('/rnlab-app-control/v1/reviews', data);

/**
 *  Get product variations
 * @returns {*}
 */
export const getVariations = (product_id, lang, cancelToken) =>
  request.get(
    `/wc/v3/products/${product_id}/variations?lang=${lang}&per_page=100`,
    {cancelToken},
  );

/**
 *  Get product variations
 * @returns {*}
 */
export const getVariationId = (product_id, variation_id, lang, cancelToken) =>
  request.get(
    `/wc/v3/products/${product_id}/variations/${variation_id}?lang=${lang}`,
    {cancelToken},
  );
