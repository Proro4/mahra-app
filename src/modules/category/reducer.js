import {fromJS} from 'immutable';
import logger from '../../utils/logger';
import {excludeCategory} from 'src/utils/category';
import {exclude_categories} from 'src/config/category';

import * as Actions from './constants';

const initState = fromJS({
  data: [],
  loading: false,
});

export default function categoryReducer(state = initState, action = {}) {
  switch (action.type) {
    case Actions.GET_CATEGORIES:
      return state.set('loading', true);
    case Actions.SET_LOAD_FALSE:
      return state.set('loading', false);
    case Actions.GET_CATEGORIES_SUCCESS:
      logger.log('action.payload,', action.payload);
      const data = excludeCategory(action.payload, exclude_categories);
      const res = data
        .map(c => {
          return c;
        })
        .sort((a, b) => b?.sort - a?.sort);
      return state.set('loading', false).set('data', fromJS(res));
    case Actions.GET_CATEGORIES_ERROR:
      return state.set('loading', false).set('data', initState.get('data'));
    case 'UPDATE_DEMO_CONFIG_SUCCESS':
      return initState;
    default:
      return state;
  }
}
