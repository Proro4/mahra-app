import {request_custom} from 'src/utils/request';
import queryString from 'query-string';
import logger from '../../utils/logger';

/**
 * Fetch category data
 * @returns {*}
 */
export const getCategories = async query => {
  try {
    const resFetch = await request_custom.get(
      'categories?expand=parent_categories%2Cchild_categories&filter%5Bstatus%5D=1&sort=-created_at&page=1&per-page=100',
    );
    const resArr = resFetch.items;
    let res;
    if (query.lang === 'ru') {
      res = resArr.map(value => {
        return {...value, ...value.i18ns[0]};
      });
    } else {
      res = resArr.map(value => {
        return {...value, ...value.i18ns[1]};
      });
    }
    return res;
  } catch {}
};
