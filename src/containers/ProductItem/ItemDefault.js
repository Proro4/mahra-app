/* eslint-disable react-native/no-inline-styles */
import React from 'react';

import {compose} from 'recompose';
import {connect} from 'react-redux';
import moment from 'moment';

import {withNavigation} from 'react-navigation';
import {useTranslation} from 'react-i18next';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {withTheme, Image, Badge, Text} from 'src/components';
import WishListIcon from '../WishListIcon';
import Price from '../Price';

import {configsSelector, languageSelector} from 'src/modules/common/selectors';

import {white, black} from 'src/components/config/colors';
import {borderRadius, margin} from 'src/components/config/spacing';
import {mainStack} from 'src/config/navigator';
import logger from '../../utils/logger';
import {isMiddleDevice} from '../../utils/styleHelper';

export function ItemDefault(props) {
  const {item, width, height, navigation, navigationType, theme, lang} = props;
  const {thumbnails, price, is_novelty, type, id, sku, i18ns} = item;

  const on_sale = item.quantity <= 0;
  const {t} = useTranslation();

  const productItemStyle = {
    width: width,
  };

  const productItemImageStyle = {
    width,
    height,
  };

  let days = 0;

  if (item.created_at) {
    const date1 = moment();
    const date2 = moment(item.created_at);

    const diff = date1.diff(date2);
    const milliseconds = diff;
    const seconds = milliseconds / 1000;
    const minutes = seconds / 60;
    const hours = minutes / 60;
    days = hours / 24;
  }
  function characteristicPrices(item, value, index, lang){
    return (
          <View
              style={{
                // flexDirection: 'row',
                alignSelf: 'center',
                width: '100%',
              }}>
            {lang === 'ua' && (
                <View>
                  <Text h5={!isMiddleDevice} h6={isMiddleDevice}>
                    {t('empty:text_size')}: {value.i18ns[1].value};{' '}
                  </Text>
                  <Text h5={!isMiddleDevice} h6={isMiddleDevice}>
                    {t('empty:text_price')}:{' '}
                    <Text
                        h6
                        style={[
                          styles.textName,
                          {
                            color: theme.ProductItem.color,
                          },
                        ]}>
                      {t('common:opt')} (від {Number(item?.buy_min)} шт){' '}
                    </Text>
                  </Text>
                  <Text
                      style={{fontWeight: 'bold', fontSize: 17, width: '100%', flex:1}}
                      h5={!isMiddleDevice}
                      h6={isMiddleDevice}>
                    <Text>{item.characteristic_prices[index].price} ₴</Text>
                  </Text>
                </View>
            )}
            {lang !== 'ua' && (
                <View>
                  <View
                      style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                      }}>
                    <Text h6 h6Style={{ fontSize: 10 }}>{t('empty:text_size')}: </Text>
                    <Text h6 h6Style={{ fontSize: 10 }}>{value.i18ns[0].value};</Text>
                  </View>
                  <Text h6={isMiddleDevice} h6Style={{ fontSize: 10 }}>
                    {t('empty:text_price')}:{' '}
                    <Text
                        h6
                        h6Style={{ fontSize: 10 }}
                        style={[
                          styles.textName,
                          {
                            color: theme.ProductItem.color,
                          },
                        ]}>
                      {t('common:opt')} (от {item?.buy_min} шт){' '}
                    </Text>
                    <Text
                        style={{fontWeight: 'bold', fontSize: 15}}
                        h6Style={{ fontSize: 10 }}
                        h6>
                      <Text>{item.characteristic_prices[index].price} ₴</Text>
                    </Text>
                  </Text>
                </View>
            )}
          </View>
    );
  }

  return (
    <TouchableOpacity
      style={productItemStyle}
      // style={{width: '100%'}}
      onPress={() => {
        navigation[navigationType](mainStack.product, {product: item});
      }}>
      <View>
        <Image
          source={
            thumbnails && thumbnails.length
              ? {uri: thumbnails[0].url}
              : require('src/assets/images/pDefault.png')
          }
          resizeMode="cover"
          style={[productItemImageStyle, {borderRadius: 10}]}
          defaultSource={require('src/assets/images/pDefault.png')}
          PlaceholderContent={<ActivityIndicator />}
        />
        <View style={styles.labelWrap}>
          <View style={styles.viewHeaderLabel}>
            <View>
              {is_novelty || days <= 12 ? (
                  <Badge
                    value={t('common:text_new')}
                    status="success"
                    containerStyle={styles.badge}
                  />
              ) : null}
              {on_sale ? (
                <Badge value={t('common:text_sale')} status="warning" />
              ) : null}
              {sku ? <Badge value={sku} status="primary" /> : null}
            </View>
            <WishListIcon
              product_id={id}
              color="#FF0000"
              colorSelect="#FF0000"
              size={25}
            />
          </View>
        </View>
      </View>
      <View style={styles.viewInfo}>
        <Text
          h6
          h6Style={{
           lineHeight: 15
          }}
          style={[
            styles.textName,
            {
              color: theme.ProductItem.color,
            },
          ]}>
          {i18ns[lang !== 'ua' ? 0 : 1]?.name}
        </Text>
        {/* <Price price_format={price} type={type} style={styles.textPrice} /> */}
        {item.characteristics?.length > 0 &&
        item.characteristic_prices?.length > 0 ? (
          item.characteristics[0].values.map((value, index) => {
              if(index <= 1){
                return(
                    <View
                        key={`${value.id}_${index}`}
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                          marginTop: index === 0 ? 0 : 10,
                          width: '100%',
                        }}>
                      {characteristicPrices(item, value, index, lang, theme)}
                    </View>
                )
              }
          })
        ) : (
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              // marginTop: 10,
              width: '100%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                width: '70%',
              }}>
              {lang === 'ua' && (
                <View>
                  <Text
                    h6
                    style={[
                      styles.textName,
                      {
                        color: theme.ProductItem.color,
                      },
                    ]}>
                    {t('common:opt')} (от {item?.buy_min} шт){' '}
                  </Text>
                  <Text style={{fontWeight: 'bold', fontSize: 15}} h5>
                    {item.price} ₴
                  </Text>
                </View>
              )}
              {lang !== 'ua' && (
                <View>
                  <Text
                    h6
                    style={[
                      styles.textName,
                      {
                        color: theme.ProductItem.color,
                      },
                    ]}>
                    {t('common:opt')} (от {item?.buy_min} шт){' '}
                  </Text>
                  <Text style={{fontWeight: 'bold', fontSize: 15}} h5>
                    {item.price} ₴
                  </Text>
                </View>
              )}
            </View>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  viewInfo: {
    marginTop: margin.base + 2,
    width: '100%',
  },
  labelWrap: {
    position: 'absolute',
    top: 10,
    left: 10,
    right: 10,
    bottom: 12,
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  viewHeaderLabel: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textName: {
    marginBottom: 4,
  },
  textPrice: {
    marginBottom: margin.small,
  },
  badge: {
    marginBottom: 5,
  },
  viewFooter: {
    flexDirection: 'row',
    // justifyContent: '',
    alignItems: 'center',
  },
  nameRating: {
    fontSize: 10,
    lineHeight: 15,
    marginLeft: margin.small - 2,
  },
  buttonAdd: {
    backgroundColor: black,
    width: 29,
    height: 29,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: borderRadius.base,
  },
  textAdd: {
    color: white,
  },
});

ItemDefault.defaultProps = {
  width: 227,
  height: 227,
  navigationType: 'navigate',
};

const mapStateToProps = state => {
  return {
    configs: configsSelector(state),
    lang: languageSelector(state),
  };
};

export default compose(
  withTheme,
  withNavigation,
  connect(mapStateToProps),
)(ItemDefault);
