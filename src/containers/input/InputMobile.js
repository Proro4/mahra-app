import React, {Component} from 'react';
import {withTranslation} from 'react-i18next';
import {StyleSheet, View} from 'react-native';
import {Icon, withTheme} from 'src/components';
import ViewLabel, {MIN_HEIGHT} from '../ViewLabel';
import {grey4} from 'src/components/config/colors';
import {TextInputMask} from 'react-native-masked-text';

import {padding} from 'src/components/config/spacing';

class InputMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSecure: props.secureTextEntry,
      isHeading: props.value || props.defaultValue,
    };
    this.input = React.createRef();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setState({
        isHeading: this.props.value,
      });
    }
  }

  handleFocus = data => {
    this.setState({isHeading: true});
    if (this.props.onFocus) {
      this.props.onFocus(data);
    }
  };
  onChange = value => {
    this.setState(
      {
        value,
      },
      () => {
        if (this.props.onChangeText) {
          this.props.onChangeText(value);
        }
      },
    );
  };
  handleBlur = data => {
    const {value} = this.props;
    this.setState({
      isHeading:
        value || (this.input.current && this.input.current._lastNativeText),
    });
    if (this.props.onBlur) {
      this.props.onBlur(data);
    }
  };
  render() {
    const {
      label,
      error,
      style,
      textStyle,
      flagStyle,
      textProps,
      theme,
      t,
      secureTextEntry,
      multiline,
      ...rest
    } = this.props;
    const {pickerData, search, isSecure, isHeading} = this.state;

    return (
      <ViewLabel label={label} error={error} isHeading={isHeading}>
        <View style={styles.viewInput}>
          <TextInputMask
            {...rest}
            type="custom"
            options={{
              mask: '+389999999999',
            }}
            keyboardType="phone-pad"
            inputRef={this.input}
            testID="RN-text-input"
            onBlur={this.handleBlur}
            onFocus={this.handleFocus}
            secureTextEntry={isSecure}
            multiline={multiline}
            style={[
              styles.input,
              !multiline && {
                height: MIN_HEIGHT,
              },
              style && style,
            ]}
          />
          {secureTextEntry && (
            <Icon
              name={isSecure ? 'eye' : 'eye-off'}
              color={isSecure ? grey4 : theme.colors.primary}
              size={15}
              containerStyle={styles.viewIcon}
              iconStyle={styles.icon}
              underlayColor="transparent"
              onPress={() =>
                this.setState({
                  isSecure: !isSecure,
                })
              }
            />
          )}
        </View>
      </ViewLabel>
    );
  }
}

let styles = StyleSheet.create({
  input: {
    height: MIN_HEIGHT,
    paddingHorizontal: padding.large,
    color: '#000',
  },
  flag: {
    width: 30,
    height: 20,
    borderWidth: 0,
  },
  item: {
    paddingHorizontal: padding.large,
  },
  search: {
    paddingVertical: 0,
    paddingBottom: padding.small,
  },
});

InputMobile.defaultProps = {
  // initialCountry: 'sa',
  offset: padding.base,
};

export default withTranslation()(withTheme(InputMobile));
