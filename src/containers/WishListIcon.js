import React from 'react';

import {View, Text} from 'react-native'
import {connect} from 'react-redux';

import {Icon} from 'src/components';
import {grey4, black} from 'src/components/config/colors';

import {addWishList, removeWishList} from 'src/modules/common/actions';
import {configsSelector} from 'src/modules/common/selectors';
import {wishListSelector} from 'src/modules/common/selectors';

export function WishListIcon(props) {
  const {
    product_id,
    wishList,
    dispatch,
    color,
    left,
    colorSelect,
    configs,
    ...rest
  } = props;
  if (!configs.get('toggleWishlist')) {
    return null;
  }
  const hasList = wishList.has(product_id);
  const wishListAction = hasList
    ? () => dispatch(removeWishList(product_id))
    : () => dispatch(addWishList(product_id));

  return (
    <View style={{backgroundColor: '#fff', borderRadius: 50, padding:3, maxHeight:30, marginLeft: left}}>
      <Icon
        size={18}
        {...rest}
        type="font-awesome"
        name={hasList ? 'heart' : 'heart-o'}
        color={hasList ? colorSelect : color}
        onPress={wishListAction}
        underlayColor={'transparent'}
      />
    </View>
  );
}

WishListIcon.defaultProps = {
  width: 150,
  height: 150,
  color: grey4,
  colorSelect: black,
};

const mapStateToProps = state => ({
  wishList: wishListSelector(state),
  configs: configsSelector(state),
});

export default connect(mapStateToProps)(WishListIcon);
