import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Text, ThemedView} from 'src/components';
import {useTranslation} from 'react-i18next';

import {margin, borderRadius} from 'src/components/config/spacing';
import logger from '../utils/logger';
import {showMessage} from "react-native-flash-message";

const Quantity = props => {
  const { t } = useTranslation();

  const {value, onChange, style, min_quantity, buy_min} = props;

  const onAdd = () => {
    const add = value + parseInt(min_quantity, 10);

    onChange && onChange(add);
  };
  const onMinus = () => {
    const minus = value - parseInt(min_quantity, 10);
    if (buy_min <= minus) {
      onChange && onChange(minus);
    }else{
      return showMessage({
        message: t('common:buy_min_error'),
        type: 'danger',
      });
    }
  };
  return (
    <ThemedView style={[styles.container, style && style]} colorSecondary>
      <Text style={styles.text}>{value}</Text>
      <TouchableOpacity style={[styles.touch, styles.left]} onPress={onMinus}>
        <Text h4 medium>
          -
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.touch, styles.right]} onPress={onAdd}>
        <Text h4 medium>
          +
        </Text>
      </TouchableOpacity>
    </ThemedView>
  );
};

Quantity.defaultProps = {
  value: 1,
  onChange: () => {},
};

const styles = StyleSheet.create({
  container: {
    minWidth: 86,
    borderRadius: borderRadius.base,
    overflow: 'hidden',
  },
  touch: {
    width: margin.big,
    height: '100%',
    position: 'absolute',
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  left: {
    left: 0,
    paddingLeft: 4,
  },
  right: {
    right: 0,
    paddingRight: 4,
  },
  text: {
    textAlign: 'center',
    marginHorizontal: margin.big,
    paddingVertical: 4,
  },
});

export default Quantity;
