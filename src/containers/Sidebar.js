import React from 'react';
import {connect} from 'react-redux';

import {getStatusBarHeight} from 'react-native-status-bar-height';
import {
  StyleSheet,
  ScrollView,
  View,
  Dimensions,
  Platform,
  ActivityIndicator,
} from 'react-native';
import {ThemedView, Text, ListItem} from 'src/components';
import ItemCategoryMenu from './ItemCategoryMenu';
import unescape from 'lodash/unescape';

import {categorySelector} from 'src/modules/category/selectors';

import {padding, margin} from 'src/components/config/spacing';

import {homeDrawer, profileStack} from 'src/config/navigator';
import {excludeCategory} from '../utils/category';
import {exclude_categories_sidebar} from '../config/category';
import logger from '../utils/logger';

const isAndroid = Platform.OS === 'android';

const dataHelpInfo = [
  // {
  //   id: '1',
  //   name: 'common:text_home',
  //   router: homeDrawer.home_tab,
  // },
  // {
  //   id: '2',
  //   name: 'common:text_blogs',
  //   router: homeDrawer.blog,
  // },
  {
    id: '3',
    name: 'common:text_about',
    router: profileStack.about,
  },
  {
    id: '4',
    name: 'common:text_contact',
    router: profileStack.contact,
  },
  // {
  //   id: '5',
  //   name: 'common:text_privacy_full',
  //   router: profileStack.privacy,
  // },
];

class Sidebar extends React.Component {
  state = {
    openedCollapse: true,
  };

  handlePage = (router, params = {}) => {
    const {navigation} = this.props;
    if (!navigation) {
      return null;
    }
    navigation.navigate(router, params);
  };

  handleCollapse = () => {
    this.setState({openedCollapse: !this.state.openedCollapse});
  };

  render() {
    const {
      category,
      navigation,
      categoryLoad,
      screenProps: {t},
    } = this.props;
    const {data} = category;

    // Filter include category
    const _data = excludeCategory(data, exclude_categories_sidebar);

    const sortCatalog = _data
      .map(c => {
        return c;
      })
      .filter(value => value !== null)
      .sort((a, b) => b?.sort - a?.sort);

    return (
      <ThemedView isFullView>
        <ScrollView
        // contentContainerStyle={
        //   {
        //     backgroundColor: 'red',
        //     paddingBottom: 60
        //     // paddingBottom: isAndroid
        //     //   ? Dimensions.get('window').height * 0.8
        //     //   : Dimensions.get('window').height * 0.2,
        //   }
        // }
        >
          {!categoryLoad && (
            <View style={styles.container}>
              <View>
                <Text h3 medium style={[styles.title, styles.titleHead]}>
                  {t('common:text_category')}
                </Text>
                {sortCatalog.map(c => {
                  if (c === null) {
                    return null;
                  }
                  let boofer = [];
                  if (c.child_categories && c.child_categories.length > 0) {
                    _data.reverse().forEach(value => {
                      if (c.id === value.parent_id) {
                        boofer.push(value);
                      }
                      // c.child_categories.forEach(value_c => {
                      //   if (value_c === value.id) {
                      //     boofer.push(value);
                      //     return value;
                      //   }
                      // });
                    });

                    boofer.sort((a, b) =>
                      b?.sort === 0 && a.sort === 0
                        ? a.id - b.id
                        : b?.sort - a?.sort,
                    );
                  }
                  if (c.parent_categories?.length > 0) {
                    return null;
                  }
                  return (
                    <ItemCategoryMenu
                      key={c.id}
                      category={c}
                      listChildCategorys={boofer}
                      isOpen={
                        navigation.state && navigation.state.isDrawerOpen
                          ? navigation.state.isDrawerOpen
                          : false
                      }
                      handleCollapse={this.handleCollapse}
                      goProducts={this.handlePage}
                    />
                  );
                })}
              </View>
              <View>
                <Text h3 medium style={styles.title}>
                  {t('common:text_help')}
                </Text>
                {dataHelpInfo.map(value => (
                  <ListItem
                    key={value.id}
                    title={t(value.name)}
                    titleProps={{
                      medium: true,
                    }}
                    type="underline"
                    small
                    containerStyle={styles.item}
                    onPress={() => this.handlePage(value.router)}
                  />
                ))}
              </View>
            </View>
          )}
          {categoryLoad && (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
                height: Dimensions.get('window').height * 1,
                // height: '100%',
              }}>
              <ActivityIndicator size="large" color="#000" />
            </View>
          )}
        </ScrollView>
      </ThemedView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginTop: margin.big + 4,
    marginBottom: margin.small + 1,
    paddingHorizontal: padding.large,
  },
  titleHead: {
    paddingTop: getStatusBarHeight(),
  },
  item: {
    paddingHorizontal: padding.large,
  },
  container: {
    minHeight: Dimensions.get('window').height,
    // flex: 1,
    // justifyContent: 'space-between',
    paddingBottom: 40,
  },
});

const mapStateToProps = state => ({
  category: categorySelector(state),
  categoryLoad: state.category.get('loading'),
});
export default connect(mapStateToProps)(Sidebar);
