import React from 'react';
import { TouchableOpacity,View } from 'react-native';
import { Icon as IconComponent } from 'src/components';
import { withNavigation } from 'react-navigation';
import {homeDrawer, homeTabs} from "../../config/navigator";

const Icon = ({ navigation, onPress, ...rest }) => {
  const handleClick = onPress ? onPress : () => {
      if(navigation.goBack()){
        return navigation.goBack();
      }else{
          navigation.navigate(homeTabs.home)
      }
  }

  return (
    <TouchableOpacity onPress={handleClick} style={{ padding: 6}}>
        <View style={{backgroundColor: '#fff', borderRadius: 50}}>
            <IconComponent name="chevron-left" size={26} isRotateRTL {...rest} />
        </View>
    </TouchableOpacity>
  );
};

export default withNavigation(Icon);
