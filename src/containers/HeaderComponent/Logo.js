import React from 'react';
import {connect} from 'react-redux';
import {withNavigation} from 'react-navigation';

import {TouchableOpacity, Image, Text, StyleSheet} from 'react-native';

import {listImageSelector} from 'src/modules/common/selectors';
import {homeDrawer} from 'src/config/navigator';

const Logo = ({images, navigation, ...rest}) => {
  return (
    <TouchableOpacity onPress={() => navigation.navigate(homeDrawer.home_tab)}>
      <Image
        // source={images.logo}
        style={{width: 130, height: 130}}
        resizeMode="stretch"
        {...rest}
        source={require('src/assets/images/logoTwo/Mahra_logo_ukr.png') }
      />
      {/* <Text style={StyleSheet.flatten([
          {fontSize: 24},
        ])}>Mahra</Text> */}
    </TouchableOpacity>
  );
};

const mapStateToProps = state => {
  return {
    images: listImageSelector(state),
  };
};

export default connect(mapStateToProps)(withNavigation(Logo));
