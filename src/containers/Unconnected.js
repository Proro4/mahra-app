import React from 'react';
import {StyleSheet, Image} from 'react-native';
import {ThemedView} from 'src/components';
import Empty from './Empty';
import {margin} from 'src/components/config/spacing';
import {languageSelector} from 'src/modules/common/selectors';
import {connect} from 'react-redux';

const Unconnected = ({clickTry, lang}) => {
  console.log('lang', lang);
  return (
    <ThemedView isFullView>
      <Empty
        title={
          lang === 'ua'
            ? 'Відсутнє підключення до Інтернету'
            : 'Отсутствует подключение к Интернету'
        }
        subTitle={
          lang === 'ua'
            ? 'Перевірте підключення до Інтернету та повторіть спробу'
            : 'Проверьте подключение к Интернету и повторите попытку'
        }
        avatarElement={
          <ThemedView colorSecondary style={styles.avatar}>
            <Image
              source={require('src/assets/images/unconnected.png')}
              resizeMode="stretch"
            />
          </ThemedView>
        }
        titleButton={
          lang === 'ua' ? 'Спробувати ще раз' : 'Попробовать еще раз'
        }
        clickButton={clickTry ? clickTry : () => {}}
        buttonProps={{
          type: 'solid',
        }}
      />
    </ThemedView>
  );
};

const styles = StyleSheet.create({
  avatar: {
    width: 95,
    height: 95,
    borderRadius: 47.5,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: margin.big + 4,
  },
});

const mapStateToProps = state => {
  return {
    lang: languageSelector(state),
  };
};

export default connect(mapStateToProps)(Unconnected);
