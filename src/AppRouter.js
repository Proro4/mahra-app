/**
 *
 * App router
 *
 *
 * App Name:          rn_oreo
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.1.0
 * Author:            Rnlab.io
 *
 * @since             1.0.0
 *
 * @format
 * @flow
 */

import React from 'react';

import './config-i18n';

import AsyncStorage from '@react-native-community/async-storage';

import {StatusBar} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {withTranslation} from 'react-i18next';
import {connect} from 'react-redux';
import {compose} from 'redux';
import merge from 'lodash/merge';

import FlashMessage from 'react-native-flash-message';

import lightColors, {darkColors} from './components/config/colors';

import {ThemeProvider} from 'src/components';
import Router from './navigation/root-switch';
import Unconnected from './containers/Unconnected';
import {setUserData} from 'src/modules/auth/actions';
import {getProfileData} from 'src/modules/auth/service';
import {fetchTemplate} from 'src/modules/common/service';

import NavigationService from 'src/utils/navigation';
import {
  themeSelector,
  colorsSelector,
  languageSelector,
} from './modules/common/selectors';

import {setOrderData} from 'src/modules/order/actions';
import logger from './utils/logger';

class AppRouter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isCheck: false,
      isConnected: true,
    };
  }

  async getUserData() {
    try {
      // const res = await fetchTemplate();
      // console.log('fetchTemplate', res);
      const data_order = await AsyncStorage.getItem('@data_order');
      logger.log('data_order', JSON.parse(data_order));
      if (data_order && data_order.length > 0) {
        this.props.dispatch(setOrderData(JSON.parse(data_order)));
      }
      const getData = await getProfileData();
      this.props.dispatch(setUserData(getData));
    } catch (e) {
      logger.log('auth err', e);
    }
  }

  componentDidMount() {
    this.getUserData();

    NetInfo.addEventListener(state => {
      const {isCheck} = this.state;
      const {isConnected} = state;
      if (!isConnected) {
        this.setState({
          isConnected: false,
        });
      }
      if (isCheck && isConnected) {
        this.setState({
          isConnected: true,
          isCheck: false,
        });
      }
    });
  }

  checkInternet = () => {
    this.setState({
      isCheck: true,
    });
  };

  render() {
    const {theme, colors, language, t, i18n} = this.props;
    const {isConnected} = this.state;

    // Change language
    if (i18n.language !== language) {
      i18n.changeLanguage(language);
    }

    const themeColor =
      theme === 'light' ? merge(lightColors, {colors}) : darkColors;
    const barStyle = theme === 'light' ? 'dark-content' : 'light-content';

    return (
      <ThemeProvider theme={themeColor}>
        <StatusBar
          translucent
          barStyle={barStyle}
          backgroundColor="transparent"
        />
        {!isConnected ? (
          <Unconnected clickTry={this.checkInternet} />
        ) : (
          <Router
            screenProps={{t, i18n, theme: themeColor}}
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        )}
        <FlashMessage position="top" />
      </ThemeProvider>
    );
  }
}

const mapStateToProps = state => {
  return {
    language: languageSelector(state),
    theme: themeSelector(state),
    colors: colorsSelector(state),
  };
};

export default compose(
  withTranslation(),
  connect(mapStateToProps),
)(AppRouter);
