import React, { useEffect } from 'react';
import { Alert, View, Text } from 'react-native';
import messaging from '@react-native-firebase/messaging';


export const TestNotification = () => {
    Alert.alert('It`s work!')
    function App() {
        useEffect(() => {
            const unsubscribe = messaging().onMessage(async remoteMessage => {
                Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
            });

            return unsubscribe;
        }, []);
    }
    return (
        <View>
            <Text style={{color:'#000'}}>
                notification
            </Text>
        </View>
    )
}
