v1.3.9

- Added: .gitingore file
- Added: Config allow customers to place orders without an account
- Added: Option config default phone number register page
- Added: Option cache data for medium to large website
- Improvement: Update user address
- Improvement: Option change add/edit fonts
- Fixed: Show image when user select variation
- Fixed: Show price currency on filter
- Fixed: Show all product when user click to action link

v1.4.0

- Fixed: Exclude parent category in style 4
- Fixed: Register Firebase SMS OTP
- Fixed: Not show Apple login when disable SMS, Google and Facebook method
- Update: To latest all dependencies

v1.4.1

- Added: Logout Apple in background
- Fixed: Auto fill OTP on Android
- Fixed: Text strings must be rendered within a <Text> component
- Fixed: Loading shipping method

v1.4.2

- Fixed: Get product min price and max price
- Add: DisableReviewRating in configs
- Fixed: Unescape html for name category
- Fixed: Cancel order on Webview
- Add: Option enable webview in product description
- Add: Support Digits plugin
